<?php

use App\Http\Controllers\ContentEventController;
use App\Http\Controllers\DashboardProduct;
use App\Http\Controllers\InvoiceHistoryController;
use App\Http\Controllers\JenisUsahaControler;
use App\Http\Controllers\KelompokUsahaController;
use App\Http\Controllers\MemberLapakUsahaKita;
use App\Http\Controllers\PointController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\ReportingController;

use App\Http\Controllers\TestingController;
use Illuminate\Support\Facades\Route;


// routing profil user 

Route::get('/profile', [ProfilController::class, 'index'])->name('profileuser');

// akhir routing profil user



Route::get('/', 'LandingPageController@index');
Route::get('/selecttopic', 'LoginControllerNew@topikContentUser')->name('selecttopic');
Route::get('/registeruser', 'LoginControllerNew@register')->name('registeruser');
Route::get('/bukausaha', 'DashboardController@index')->name('bukausaha');
Route::get('/bukausaha_detail_topik/{id}', 'DashboardController@view_detail_content_one')->name('bukausaha_detail_topik');
Route::get('/bukausaha_detail/{id}', 'DashboardController@view_detail_content')->name('bukausaha_detail');
Route::get('/manageuser', 'LoginControllerNew@index')->name('manageuser');





// invoice url
Route::get('/invoice/{id}', [InvoiceHistoryController::class, 'index'])->name('invoice');
Route::get('/generatepdf', [InvoiceHistoryController::class, 'generatepdf'])->name('invoice');



// manage content event
Route::get('/managecontent/{id}', [ContentEventController::class, 'index'])->name('managecontent');


// manage url rencana kegiatan

Route::prefix('/rencana')->group(function () {
    Route::get('/activityliburan/{id}', 'ActivityLiburanController@activity_liburan_view');
    Route::get('/rencanakegiatan', 'RencanaKegiatanController@index');
    Route::get('/alatkegiatan', 'AlatKengiantanController@index');
    Route::get('/notifikasikegiatan/{id}', 'NotifikasiKegiatanController@index');
});

// manage url point

Route::get('/pointlist', [PointController::class, 'index'])->name('pointlist');

// manage url reporting

Route::get('/reporting', [ReportingController::class, 'index'])->name('reporting');
Route::get('/detailed', [ReportingController::class, 'detailed'])->name('detailed');



// manager url product

Route::get('/product/{id}', [ProductController::class, 'index']);


// manage url jenisusaha
Route::get('/jenisusaha', [JenisUsahaControler::class, 'index']);

// manage url kelompokusaha
Route::get('/kelompokusaha', [KelompokUsahaController::class, 'index']);

Route::get('/test', [TestingController::class, 'index']);

Route::get('/member/detail/{id}', [MemberLapakUsahaKita::class, 'index_detail']);

Route::get('/bukausahaproduct', [DashboardProduct::class, 'index']);
Route::get('/bukausahaproduct/detail/{id}', [DashboardProduct::class, 'index_detail']);




// for all member
Route::get('/member', [MemberLapakUsahaKita::class, 'index']);
