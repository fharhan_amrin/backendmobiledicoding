<?php

use App\Http\Controllers\AlatKengiantanController;
use App\Http\Controllers\JenisUsahaControler;
use App\Http\Controllers\KelompokUsahaController;
use App\Http\Controllers\NotifikasiKegiatanController;
use App\Http\Controllers\RencanaKegiatanController;

use App\Http\Controllers\ContentEventController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\InvoiceHistoryController;
use App\Http\Controllers\DashboardProduct;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



// Login fungsi & data Login User
Route::prefix('/user')->group(function () {
    Route::get('/all', 'LoginControllerNew@getAllDataUser');
    Route::post('/create', 'LoginControllerNew@store');
    Route::get('/findUser/{id}', 'LoginControllerNew@findUser');
    Route::put('/update/{id}', 'LoginControllerNew@update');
    Route::delete('/delete/{id}', 'LoginControllerNew@delete');
    Route::post('/login', 'LoginControllerNew@login');
});

// Data Table Activity || controller ActivityLiburanController
Route::prefix('/activityliburan')->group(function () {
    Route::get('/all', 'ActivityLiburanController@index');
    Route::post('/store_new', 'ActivityLiburanController@store_new');
    Route::get('/findOneActivity/{id}', 'ActivityLiburanController@findOneActivity');
    Route::put('/update/{id}', 'ActivityLiburanController@update');
    Route::delete('/delete/{id}', 'ActivityLiburanController@delete');
});


// rencana kengiantan 
Route::prefix('/content')->group(function () {
    Route::get('/allDataContent', [ContentEventController::class, 'allDataContent']);
    Route::post('/store', [ContentEventController::class, 'store']);
    Route::get('/edit/{id}', [ContentEventController::class, 'edit']);
    Route::get('/edit_invoice/{id}', [ContentEventController::class, 'edit_invoice']);
    Route::post('/update', [ContentEventController::class, 'update']);
    Route::delete('/delete/{id}', [ContentEventController::class, 'destroy']);
});


// rencana kengiantan 
Route::prefix('/rencana')->group(function () {
    Route::get('/allDataRencana', [RencanaKegiatanController::class, 'allDataRencana']);
    Route::post('/store', [RencanaKegiatanController::class, 'store']);
    Route::get('/edit/{id}', [RencanaKegiatanController::class, 'edit']);
    Route::put('/update', [RencanaKegiatanController::class, 'update']);
    Route::delete('/delete/{id}', [RencanaKegiatanController::class, 'destroy']);
});
// alat alat liburan 
Route::prefix('/alat')->group(function () {
    Route::get('/allDataAlat', [AlatKengiantanController::class, 'allDataAlat']);
    Route::post('/store', [AlatKengiantanController::class, 'store']);
    Route::get('/edit/{id}', [AlatKengiantanController::class, 'edit']);
    Route::put('/update', [AlatKengiantanController::class, 'update']);
    Route::delete('/delete/{id}', [AlatKengiantanController::class, 'destroy']);
});
// Notifikasi 
Route::prefix('/notifikasi')->group(function () {
    Route::get('/AllDataNotifikasi', [NotifikasiKegiatanController::class, 'AllDataNotifikasi']);
    Route::post('/store', [NotifikasiKegiatanController::class, 'store']);
    Route::get('/edit/{id}', [NotifikasiKegiatanController::class, 'edit']);
    Route::put('/update', [NotifikasiKegiatanController::class, 'update']);
    Route::delete('/delete/{id}', [NotifikasiKegiatanController::class, 'destroy']);
});




// kelompok usaha
Route::prefix('/kelompokusaha')->group(function () {
    Route::get('/getDatakelompokUsaha', [KelompokUsahaController::class, 'getDatakelompokUsaha']);
    Route::post('/store_data', [KelompokUsahaController::class, 'store']);
    Route::get('/edit_usaha/{id}', [KelompokUsahaController::class, 'edit_usaha']);
    Route::put('/update', [KelompokUsahaController::class, 'update']);
    Route::delete('/delete/{id}', [KelompokUsahaController::class, 'destroy']);
});

// Jenis Usaha
Route::prefix('/jenisusaha')->group(function () {
    Route::get('/getDatajenisusaha', [JenisUsahaControler::class, 'getDatajenisusaha']);
    Route::post('/store', [JenisUsahaControler::class, 'store']);
    Route::get('/edit/{id}', [JenisUsahaControler::class, 'edit']);
    Route::put('/update', [JenisUsahaControler::class, 'update']);
    Route::delete('/delete/{id}', [JenisUsahaControler::class, 'destroy']);
});

// Product
Route::prefix('/product')->group(function () {
    Route::get('/getDatajenisusaha', [ProductController::class, 'getDatajenisusaha']);
    Route::post('/store', [ProductController::class, 'store']);
    Route::get('/edit/{id}', [ProductController::class, 'edit']);
    Route::post('/update', [ProductController::class, 'update']);
    Route::delete('/delete/{id}', [ProductController::class, 'destroy']);
});



// invoice history
Route::prefix('/invoicehistory')->group(function () {

    Route::post('/store', [InvoiceHistoryController::class, 'store']);

    Route::delete('/delete/{id}', [InvoiceHistoryController::class, 'destroy']);
});


Route::post('/addInvoice', [DashboardProduct::class, 'addInvoice']);
Route::post('/addInvoice_content', [DashboardProduct::class, 'addInvoice_content']);


Route::get('/contentTopik/{id}', [ContentEventController::class, 'contentTopik']);
