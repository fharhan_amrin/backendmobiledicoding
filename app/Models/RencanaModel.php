<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RencanaModel extends Model
{
    use HasFactory;
    protected $table = 'kegiatan';
    protected $fillable = ['id', 'name_kegiatan', 'created_at', 'updated_at'];
}
