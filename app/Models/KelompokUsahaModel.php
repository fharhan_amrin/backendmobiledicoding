<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KelompokUsahaModel extends Model
{
    use HasFactory;
    public $table = 'kelompok_usaha';
    public $fillable = ['id', 'id_user', 'nama_usaha', 'created_at', 'updated_at'];
}
