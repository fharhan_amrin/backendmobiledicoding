<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SosialMediaModel extends Model
{
    use HasFactory;

    protected $table = 'sosial_media';
    protected $fillable = ['id_user', 'name_sosial_media', 'created_at', 'updated_at'];
}
