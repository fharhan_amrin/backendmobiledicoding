<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PembelianModel extends Model
{
    use HasFactory;
    protected $table = 'pembeli';
    protected $fillable = ['nama', 'umur', 'notelepon', 'alamat', 'id_point', 'created_at', 'updated_at', 'pembeli'];
}
