<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentEventModel extends Model
{
    use HasFactory;
    protected $table = 'contentevent';
    protected $fillable = ['id', 'id_user_pembeli', 'id_user_pembuat', 'type_jenis_content', 'judul_event', 'deskripsi_event', 'image_event', 'link_vidio_penjelasan_event', 'link_vidio_free_hiburan', 'link_event', 'harga', 'diskon', 'status_content', 'kapasitas_penonton', 'created_at', 'updated_at'];
}
