<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceModel extends Model
{
    use HasFactory;
    protected $table = 'invoice_history';
    protected $fillable = ['id', 'id_user', 'image_bukti_pembayaran', 'total', 'id_content', 'id_product', 'invoice', 'invoice_start', 'invoice_end', 'status', 'created_at', 'updated_at'];
}
