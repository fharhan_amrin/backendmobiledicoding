<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    use HasFactory;
    protected $table = 'product';
    protected $fillable = ['id', 'id_user', 'image_product', 'name_product', 'harga_product', 'stock_product', 'status_product', 'id_jenis_usaha', 'name_jenis_usaha', 'deskripsi_penjelasan_product', 'created_at', 'updated_at'];
}
