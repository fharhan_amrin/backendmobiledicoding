<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityLiburanModel extends Model
{
    use HasFactory;
    protected $table = "activityliburan";
    protected $fillable = ['id', 'iduser', 'to', 'berapa_hari', 'type_of_trip', 'id_name_rencana_kegiatan', 'id_alatkegiatan', 'created_at', 'updated_at'];
}
