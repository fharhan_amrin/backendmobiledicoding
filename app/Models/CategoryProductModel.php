<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryProductModel extends Model
{
    use HasFactory;

    protected $table = 'category_product';
    protected $fillable =  ['id', 'name_category', 'harga_normal', 'harga_diskon', 'start_diskon', 'end_diskon', 'created_at', 'updated_at'];
}
