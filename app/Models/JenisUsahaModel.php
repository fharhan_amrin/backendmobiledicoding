<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisUsahaModel extends Model
{
    use HasFactory;
    public $table = 'jenis_usaha';
    public $fillable = ['id_user', 'name_jenis_usaha', 'deskripsi', 'created_at', 'updated_at'];
}
