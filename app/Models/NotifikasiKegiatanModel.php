<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;

class NotifikasiKegiatanModel extends Model
{
    use HasFactory, Notifiable;
    protected $table = 'notifikasikegiatan';
    protected $fillable = ['id', 'id_user', 'id_activity_kengiatan', 'start_date', 'end_date', 'per', 'status_pesan', 'notifikasi_via', 'created_at', 'updated_at'];
}
