<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlatKegiatanModel extends Model
{
    use HasFactory;
    protected $table = "alatkegiatan";
    protected $fillabel = ['id', 'name_alat', 'created_at', 'updated_at'];
}
