<?php

namespace App\Http\Controllers;

use App\Models\ProductModel;
use Illuminate\Http\Request;
use DataTables;

class ProductController extends Controller
{

    // /0==tersedia,1=habis ,2= sedang di perjalanan

    public function index(Request $request, $id)
    {

        if ($request->ajax()) {
            $data = ProductModel::select('*')->where('id_user', $id);
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image_product', function ($row) {
                    $url = asset("uploads/product/" . $row['image_product']);
                    return '<img src=' . $url . ' border="0" width="40" class="img-rounded" align="center" />';
                })->addColumn('status_product', function ($row) {
                    return $this->isStatusProduct($row['status_product']);
                })
                ->addColumn('action', function ($row) {

                    $btn = '<button type="button" onclick="getdataedit(' . $row['id'] . ')" class="btn btn-primary" data-toggle="modal" data-target="#updateactivity">
                              <i style="color:white;" class="fa fa-edit"></i>
                             </button>

                             <button type="button" onclick="deletedata(' . $row['id'] . ')" class="btn btn-danger" >
                             <i style="color:white;" class="fa fa-trash"></i>
                             </button>';

                    return $btn;
                })
                ->rawColumns(['image_product', 'status_product', 'action'])
                ->make(true);
        }
        return view('contents.product.view_product');
    }


    function isStatusProduct($status)
    {

        if ($status == 0) {
            return ' <span class="label label-info">Tersedia</span>';
        } else if ($status == 1) {
            return '<span class="label label-warning">Habis</span>';
        } else {
            return '<span class="label label-warning">Segera Hadir</span>';
        }
    }
    public function store(Request $request)
    {
        $product                    = new  ProductModel();

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('image_product');

        // nama file
        $namefile =  str_replace(" ", "", $file->getClientOriginalName());

        // // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = './uploads/product';

        // // upload file
        $file->move($tujuan_upload, str_replace(" ", "", $file->getClientOriginalName()));


        $product->id_user           = $request->id_user;
        $product->image_product     = $namefile;
        $product->name_product      = $request->name_product;
        $product->harga_product     = $request->harga_product;
        $product->stock_product     = $request->stock_product;
        $product->status_product    = $request->status_product;
        $product->id_jenis_usaha    = $request->id_jenis_usaha;
        $product->name_jenis_usaha  = $request->name_jenis_usaha;
        $product->deskripsi_penjelasan_product  = $request->deskripsi_penjelasan_product;


        $simpan =  $product->save();

        if ($simpan) {
            $result = array('status' => true, 'msg' => 'Data Berhasil Di Simpan');
        } else {
            $result = array('status' => false, 'msg' => 'Failed Insert Data');
        }


        return response()->json($result);
    }

    public function getAllDataProduct()
    {
        $query = ProductModel::all();

        if ($query) {
            $result = array('status' => true, 'data' => $query);
        } else {
            $result  = array('status' => false, 'msg' => 'failed get data product all');
        }

        return response()->json($result);
    }


    public function edit($id)
    {
        $product = ProductModel::find($id);

        if ($product == '') {
            $result = array('status' => false, 'msg' => 'Id kosong', 'data' => $product);
        } else {
            if ($product) {
                $result = array('status' => true, 'data' => $product);
            } else {
                $result = array('status' => false);
            }
        }

        return response()->json($result);
    }

    public function update(Request $request)
    {
        $product =  ProductModel::find($request->id);

        if ($product == '') {
            $result = array('status' => true, 'msg' => 'id Tidak ada di database');
        } else {
            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('image_product');

            // nama file
            $namefile =  str_replace(" ", "", $file->getClientOriginalName());

            // // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = './uploads/product';

            // // upload file
            $file->move($tujuan_upload, str_replace(" ", "", $file->getClientOriginalName()));


            $product->id                            = $request->id;
            $product->image_product                 = $namefile;
            $product->name_product                  = $request->name_product;
            $product->harga_product                 = $request->harga_product;
            $product->stock_product                 = $request->stock_product;
            $product->status_product                = $request->status_product;
            $product->id_jenis_usaha                = $request->id_jenis_usaha;
            $product->name_jenis_usaha              = $request->name_jenis_usaha;
            $product->deskripsi_penjelasan_product  = $request->deskripsi_penjelasan_product;

            $simpan =  $product->save();

            if ($simpan) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Update Data');
            }
        }

        return response()->json($result);
    }


    public function destroy($id)
    {
        $product =  ProductModel::find($id);

        if ($product == '') {
            $result = array('status' => false, 'msg' => 'id anda yang mau di hapus, tidak tersedia di database');
        } else {
            $delete = $product->delete();
            if ($delete) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Delete');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Deletea Data');
            }
        }

        return response()->json($result);
    }
}
