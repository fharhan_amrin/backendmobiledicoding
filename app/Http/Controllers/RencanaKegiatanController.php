<?php

namespace App\Http\Controllers;

use App\Models\RencanaModel;
use Illuminate\Http\Request;
use DataTables;

class RencanaKegiatanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = RencanaModel::select('*');
            return Datatables::of($data)
                ->addIndexColumn()
                // ->addColumn('name_kegiatan', function ($row) {
                //     return $row['name_kegiatan'] == '' ? '-' : $row['name_kegiatan'];
                // })

                ->addColumn('action', function ($row) {

                    $btn = '<button type="button" onclick="getdataedit(' . $row['id'] . ')" class="btn btn-primary" data-toggle="modal" data-target="#updateactivity">
                              <i style="color:white;" class="fa fa-edit"></i>
                             </button>

                             <button type="button" onclick="deletedata(' . $row['id'] . ')" class="btn btn-danger" >
                              <i style="color:white;" class="fa fa-trash"></i>
                             </button>';

                    return $btn;
                })
                ->rawColumns(['action'])
                // ->removeColumn('id')
                ->make(true);
        }
        return view('contents.activityliburan.activity_view_rencana_kegiatan');
    }
    public function store(Request $request)
    {
        $rencana = new  RencanaModel();


        $rencana->name_kegiatan = $request->name_kegiatan;

        $simpan =  $rencana->save();

        if ($simpan) {
            $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
        } else {
            $result = array('status' => false, 'msg' => 'Failed Update Data');
        }


        return response()->json($result);
    }

    public function allDataRencana()
    {
        $query = RencanaModel::all();

        if ($query) {
            $result = array('status' => true, 'data' => $query);
        } else {
            $result  = array('status' => false, 'msg' => 'failed get data Sosial Media');
        }

        return response()->json($result);
    }


    public function edit($id)
    {
        $rencana = RencanaModel::find($id);

        if ($rencana == '') {
            $result = array('status' => false, 'msg' => 'Id kosong', 'data' => $rencana);
        } else {
            if ($rencana) {
                $result = array('status' => true, 'data' => $rencana);
            } else {
                $result = array('status' => false);
            }
        }

        return response()->json($result);
    }

    public function update(Request $request)
    {
        $rencana =  RencanaModel::find($request->id);

        if ($rencana == '') {
            $result = array('status' => true, 'msg' => 'id Tidak ada di database');
        } else {
            $rencana->name_kegiatan = $request->name_kegiatan;

            $simpan =  $rencana->save();

            if ($simpan) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Update Data');
            }
        }

        return response()->json($result);
    }


    public function destroy($id)
    {
        $rencana =  RencanaModel::find($id);

        if ($rencana == '') {
            $result = array('status' => false, 'msg' => 'id anda yang mau di hapus, tidak tersedia di database');
        } else {
            $delete = $rencana->delete();
            if ($delete) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Delete');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Deletea Data');
            }
        }

        return response()->json($result);
    }
}
