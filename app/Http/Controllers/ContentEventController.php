<?php

namespace App\Http\Controllers;

use App\Models\ContentEventModel;
use App\Models\InvoiceModel;
use Illuminate\Http\Request;

use DataTables;

class ContentEventController extends Controller
{


    public function index(Request $request, $id)
    {
        $id_user_pembuat = $id;


        if ($request->ajax()) {
            $data = ContentEventModel::select('*')->where('id_user_pembuat', $id_user_pembuat);
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {

                    $btn = '

                    <button type="button" onclick="getdatadetail(' . $row['id'] . ')" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalDetail">
                              <i style="color:white;" class="fa fa-eye"></i>
                             </button>
                    <button type="button" onclick="getdataedit(' . $row['id'] . ')" class="btn btn-primary" data-toggle="modal" data-target="#exampleModaledit">
                              <i style="color:white;" class="fa fa-edit"></i>
                             </button>

                             <button type="button" onclick="deletedata(' . $row['id'] . ')" class="btn btn-danger" >
                              <i style="color:white;" class="fa fa-trash"></i>
                             </button>';

                    return $btn;
                })
                ->rawColumns(['action'])
                // ->removeColumn('id')
                ->make(true);
        }
        return view('contents.topikContent.conten_view');
    }
    public function store(Request $request)
    {
        $content = new  ContentEventModel();

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('image_event_type');

        // nama file
        $namefile =  str_replace(" ", "", $file->getClientOriginalName());

        // // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = './uploads';

        // // upload file
        $file->move($tujuan_upload, str_replace(" ", "", $file->getClientOriginalName()));


        $content->id_user_pembuat               = $request->id;
        $content->judul_event                   = $request->judul_event;
        $content->deskripsi_event               = $request->deskripsi_event;
        $content->image_event                   = $namefile;
        $content->link_vidio_penjelasan_event   = $request->link_vidio_penjelasan_event;
        $content->link_vidio_free_hiburan       = $request->link_vidio_free_hiburan;
        $content->link_event                    = $request->link_event;
        $content->harga                         = $request->harga;
        $content->diskon                        = $request->diskon;
        $content->status_content                = $request->status_content;
        $content->type_jenis_content            = $request->type_jenis_content;
        $content->kapasitas_penonton            = $request->kapasitas_penonton;

        $simpan                                 =  $content->save();

        if ($simpan) {
            $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
        } else {
            $result = array('status' => false, 'msg' => 'Failed Update Data');
        }


        return response()->json($result);
    }

    public function allDataContent()
    {
        $query = ContentEventModel::all();

        if ($query) {
            $result = array('status' => true, 'data' => $query);
        } else {
            $result  = array('status' => false, 'msg' => 'failed get data Sosial Media');
        }

        return response()->json($result);
    }


    public function edit($id)
    {
        $content = ContentEventModel::find($id);

        if ($content == '') {
            $result = array('status' => false, 'msg' => 'Id kosong', 'data' => $content);
        } else {
            if ($content) {
                $result = array('status' => true, 'data' => $content);
            } else {
                $result = array('status' => false);
            }
        }

        return response()->json($result);
    }

    public function edit_invoice($id)
    {
        $content = InvoiceModel::find($id);

        if ($content == '') {
            $result = array('status' => false, 'msg' => 'Id kosong', 'data' => $content);
        } else {
            if ($content) {
                $result = array('status' => true, 'data' => $content);
            } else {
                $result = array('status' => false);
            }
        }

        return response()->json($result);
    }

    public function update(Request $request)
    {
        $content =  ContentEventModel::find($request->id);

        if ($content == '') {
            $result = array('status' => true, 'msg' => 'id Tidak ada di database');
        } else {

            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('image_event_type_edit');

            // nama file
            $namefile =  str_replace(" ", "", $file->getClientOriginalName());

            // // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = './uploads';

            // // upload file
            $file->move($tujuan_upload, str_replace(" ", "", $file->getClientOriginalName()));

            $content->id                            = $request->id;

            $content->judul_event                   = $request->judul_event;
            $content->deskripsi_event               = $request->deskripsi_event;
            $content->image_event                   = $namefile;
            $content->link_vidio_penjelasan_event   = $request->link_vidio_penjelasan_event;
            $content->link_vidio_free_hiburan       = $request->link_vidio_free_hiburan;
            $content->link_event                    = $request->link_event;
            $content->harga                         = $request->harga;
            $content->diskon                        = $request->diskon;
            $content->status_content                = $request->status_content;
            $content->type_jenis_content            = $request->type_jenis_content;
            $content->kapasitas_penonton            = $request->kapasitas_penonton;



            $simpan =  $content->save();

            if ($simpan) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Update Data');
            }
        }

        return response()->json($result);
    }


    public function destroy($id)
    {
        $content =  ContentEventModel::find($id);

        if ($content == '') {
            $result = array('status' => false, 'msg' => 'id anda yang mau di hapus, tidak tersedia di database');
        } else {
            $delete = $content->delete();
            if ($delete) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Delete');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Deletea Data');
            }
        }

        return response()->json($result);
    }

    public function contentTopik($id)
    {
        $content = ContentEventModel::where('type_jenis_content', $id)->get();

        if ($content == '') {
            $result = array('status' => false, 'msg' => 'Id kosong', 'data' => $content);
        } else {
            if ($content) {
                $result = array('status' => true, 'data' => $content);
            } else {
                $result = array('status' => false);
            }
        }

        return response()->json($result);
    }
}
