<?php

namespace App\Http\Controllers;

use App\Models\ActivityLiburanModel;
use Illuminate\Http\Request;
use DataTables;

class ActivityLiburanController extends Controller
{

    public function activity_liburan_view(Request $request, $id)
    {

        if ($request->ajax()) {
            $data = ActivityLiburanModel::select('*')->where('iduser', $id);
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {

                    $btn = '<button type="button" onclick="getdataedit(' . $row['id'] . ')" class="btn btn-primary" data-toggle="modal" data-target="#updateactivity">
                              <i style="color:white;" class="fa fa-edit"></i>
                             </button>

                             <button type="button" onclick="deletedata(' . $row['id'] . ')" class="btn btn-danger" >
                              <i style="color:white;" class="fa fa-trash"></i>
                             </button>';

                    return $btn;
                })
                ->rawColumns(['action'])
                // ->removeColumn('id')
                ->make(true);
        }
        return view('contents.activityliburan.activity_liburan_view');
    }




    public function index()
    {
        $query = ActivityLiburanModel::all();

        if ($query) {
            $result = array('status' => true, 'data' => $query);;
        } else {
            $result  = array('status' => false, 'msg' => 'failed get data');
        }

        return response()->json($result);
    }

    public function store_new(Request $request)
    {


        $array  = $request->id_name_rencana_kegiatan;
        $x  = count($request->id_name_rencana_kegiatan);

        $arrayalatkegiatan  = $request->id_alatkegiatan;
        $xalatkegiatan  = count($request->id_alatkegiatan);

        $nilai = '';
        $nilaialatkegiatan = '';

        for ($i = 0; $i < $x; $i++) {

            if ($array[$i] != end($array)) {
                (int)$nilai .= (int)$array[$i] . ",";
            } else {
                (int)$nilai .= (int)$array[$i];
            }
        }
        for ($i = 0; $i < $xalatkegiatan; $i++) {

            if ($arrayalatkegiatan[$i] != end($arrayalatkegiatan)) {
                (int)$nilaialatkegiatan .= (int)$arrayalatkegiatan[$i] . ",";
            } else {
                (int)$nilaialatkegiatan .= (int)$arrayalatkegiatan[$i];
            }
        }
        $x = new  ActivityLiburanModel();
        $x->to  = $request->to;
        $x->type_of_trip  = $request->type_of_trip;
        $x->berapa_hari  = $request->berapa_hari;
        $x->iduser  = $request->iduser;
        $x->id_alatkegiatan  = $nilaialatkegiatan;

        $x->id_name_rencana_kegiatan = $nilai;
        $simpan =  $x->save();


        if ($simpan) {
            $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
        } else {
            $result = array('status' => false, 'msg' => 'Failed Update Data');
        }


        return response()->json($result);
    }

    public function findOneActivity($id)
    {
        $query  = ActivityLiburanModel::find($id);

        if ($query == '' || $query == null) {
            $result  = array('status' => false, 'msg' => 'Data id Anda Masukan Tidak di temukan');
        }

        if ($query) {
            $result = array('status' => true, 'data' => $query);;
        } else {
            $result  = array('status' => false, 'msg' => 'failed get data');
        }

        return response()->json($result);
    }

    public function update(Request $request, $id)
    {

        $activityliburan = ActivityLiburanModel::find($id);

        if ($activityliburan == '' || $activityliburan == null) {
            $result  = array('status' => false, 'msg' => 'Data id Anda Masukan Tidak di temukan');
        }


        $activityliburan->iduser            = $request->iduser;
        $activityliburan->id_jenis_kegiatan = $request->id_jenis_kegiatan;
        $activityliburan->id_alatkegiatan   = $request->id_alatkegiatan;

        $query  =  $activityliburan->save();

        if ($query) {
            $result  = array('status' => true, 'data' => $query);;
        } else {
            $result  = array('status' => false, 'msg' => 'failed get data');
        }


        return response()->json($result);
    }


    public function delete($id)
    {
        $activityliburan = ActivityLiburanModel::find($id);

        if ($activityliburan == '' || $activityliburan == null) {
            $result  = array('status' => false, 'msg' => 'Data id Anda Masukan Tidak di temukan');
        }
        $query = $activityliburan->delete();
        if ($query) {
            $result = array('status' => true, 'msg' => 'Success Delete Data id ' . $id);;
        } else {
            $result  = array('status' => false, 'msg' => 'failed delete data id' . $id);
        }


        return response()->json($result);
    }
}
