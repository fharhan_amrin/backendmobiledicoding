<?php

namespace App\Http\Controllers;

use App\Models\KelompokUsahaModel;
use Illuminate\Http\Request;
use  DataTables;

class KelompokUsahaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = KelompokUsahaModel::select('*');
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('nama_usaha', function ($row) {
                    return $row['nama_usaha'];
                })
                ->addColumn('created_at', function ($row) {
                    return $row['created_at'];
                })
                ->addColumn('updated_at', function ($row) {
                    return $row['updated_at'];
                })
                ->addColumn('action', function ($row) {

                    $btn = '<button type="button" onclick="getdataedit(' . $row['id'] . ')" class="btn btn-primary" data-toggle="modal" data-target="#updateactivity">
                              <i style="color:white;" class="fa fa-edit"></i>
                             </button>

                             <button type="button" onclick="deletedata(' . $row['id'] . ')" class="btn btn-danger" >
                                <i style="color:white;" class="fa fa-trash"></i>                
                             </button>';

                    return $btn;
                })
                ->rawColumns(['action'])
                // ->removeColumn('id')
                ->make(true);
        }
        return view('contents.kelompokusaha.view_kelompokusaha');
    }

    public function store(Request $request)
    {
        $sosialmedia = new  KelompokUsahaModel();


        $sosialmedia->nama_usaha = $request->nama_usaha;

        $simpan =  $sosialmedia->save();

        if ($simpan) {
            $result = array('status' => true, 'msg' => 'Data Berhasil Di insert');
        } else {
            $result = array('status' => false, 'msg' => 'Failed Update Data');
        }


        return response()->json($result);
    }

    public function getDatakelompokUsaha()
    {
        $query = KelompokUsahaModel::all();

        if ($query) {
            $result = array('status' => true, 'data' => $query);
        } else {
            $result  = array('status' => false, 'msg' => 'failed get data Sosial Media');
        }

        return response()->json($result);
    }


    public function edit_usaha($id)
    {
        $sosialmedia = KelompokUsahaModel::find($id);

        if ($sosialmedia == '') {
            $result = array('status' => false, 'msg' => 'Id kosong', 'data' => $sosialmedia);
        } else {
            if ($sosialmedia) {
                $result = array('status' => true, 'data' => $sosialmedia);
            } else {
                $result = array('status' => false);
            }
        }

        return response()->json($result);
    }

    public function update(Request $request)
    {
        $sosialmedia =  KelompokUsahaModel::find($request->id);

        if ($sosialmedia == '') {
            $result = array('status' => true, 'msg' => 'id Tidak ada di database');
        } else {
            $sosialmedia->nama_usaha = $request->nama_usaha;

            $simpan =  $sosialmedia->save();

            if ($simpan) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Update Data');
            }
        }

        return response()->json($result);
    }


    public function destroy($id)
    {
        $sosialmedia =  KelompokUsahaModel::find($id);

        if ($sosialmedia == '') {
            $result = array('status' => false, 'msg' => 'id anda yang mau di hapus, tidak tersedia di database');
        } else {
            $delete = $sosialmedia->delete();
            if ($delete) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Delete');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Deletea Data');
            }
        }

        return response()->json($result);
    }
}
