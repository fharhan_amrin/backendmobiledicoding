<?php

namespace App\Http\Controllers;

use App\Models\JenisUsahaModel;
use App\Models\PembelianModel;
use App\Models\PenjualModel;
use App\Models\Point;
use App\Models\SosialMediaModel;
use Illuminate\Http\Request;

// use App\Http\Controllers\Controller;

use App\Models\UsersModel;

class LoginControllerNew extends Controller
{
    public function index()
    {
        $data = array(
            'title' => 'Buka Usaha',
            'sub_title_one' => 'Manage User'
        );
        return view('contents.user')->with(['data' => $data]);
    }

    public function register()
    {
        return view('contents.register.register_view');
    }

    public function topikContentUser()
    {
        return view('contents.topikContent.topikContent_view');
    }


    public function getAllDataUser()
    {
        $query = UsersModel::all();

        if ($query) {
            $result = array('status' => true, 'data' => $query);
        } else {
            $result = array('status' => false, 'msg' => 'Failed Ambil Data Dari Database');
        }

        return response()->json($result);
    }

    public function findUser($id)
    {

        $query = UsersModel::find($id);

        if ($query == null or $query == '') { //lewatkan if jika data tersedia
            return response()->json(array('status' => false, 'msg' => 'id data anda tidak anda masukan'));
        }

        if ($query) {
            $result = array('status' => true, 'data' => $query);
        } else {
            $result = array('status' => false, 'msg' => 'Failed Ambil Data');
        }

        return response()->json($result);
    }

    public function store(Request $request)
    {
        // Validate the request...

        $user = new UsersModel();

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('foto_profil');

        // nama file
        $namefile =  str_replace(" ", "", $file->getClientOriginalName());

        // // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = './uploads/fotouser';

        // // upload file
        $file->move($tujuan_upload, str_replace(" ", "", $file->getClientOriginalName()));


        $user->nama                    =   $request->nama;
        $user->jenis_kelamin           =   $request->jenis_kelamin;
        $user->umur           =   $request->umur;
        $user->notelepon               =   $request->notelepon;
        $user->foto_profile            =   $namefile;
        $user->alamat                  =   $request->alamat;
        $user->email                   =   $request->email;
        $user->username                =   $request->username;
        $user->password                =   $request->password;
        $user->status_user             =   1;
        $user->level_user              =   1;
        $user->pekerjaan               =   $request->pekerjaan;
        $user->id_jenis_usaha          =   $request->id_jenis_usaha;
        $user->name_jenis_usaha        =   $request->name_jenis_usaha;
        $user->kelompok_usaha          =   $request->kelompok_usaha;
        $user->line                    =   $request->line;
        $user->twiter                  =   $request->twiter;
        $user->instagram               =   $request->instagram;
        $user->facebook                =   $request->facebook;
        $user->tiktok                  =   $request->tiktok;
        

        $simpan                                       =    $user->save();

        if ($simpan) {
            $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
        } else {
            $result = array('status' => false, 'msg' => 'Failed Update Data');
        }




        return response()->json($result);
    }

    public function update(Request $request, $id)
    {
        // cari data berdasarkan primary key
        $user = UsersModel::find($id);
        // jika data  sama dengan kosong atau null masuk ke dalam if
        if ($user == null or $user == '') { //lewatkan if jika data tersedia
            return response()->json(array('status' => false, 'msg' => 'id data anda tidak anda masukan'));
        }
        // tangkap nilai request setelah melewati if
        $user->email       = $request->email;
        $user->username    = $request->username;
        $user->password    = $request->password;
        $user->status_user = $request->status_user;
        $user->level_user  = $request->level_user;
        $query  = $user->save(); //lakukan update data
        // check  nilai $query true || false
        if ($query) {
            $result = array('status' => true, 'msg' => 'Berhasil Update Data User id ' . $id);
        } else { //jika nilai query false
            $result = array('status' => false, 'msg' => 'Gagal Update User User');
        }
        //  kemblikan response json sesuai kondisi $query
        return response()->json($result);
    }

    public function delete($id)
    {
        // pertama select * from table where primary key=nilainya
        $user = UsersModel::find($id);

        if ($user == null or $user == '') {
            return response()->json(array('status' => false, 'msg' => 'id data anda tidak anda masukan'));
        }
        $query = $user->delete();

        if ($query) {
            $result = array('status' => true, 'msg' => 'Berhasil Mendelete Data User id ' . $id);
        } else {
            $result = array('status' => false, 'msg' => 'Gagal Mendelete User User');
        }

        return response()->json($result);
    }

    public function login(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $query   =  UsersModel::where('username', $username)->where('password', $password)->get();

        if ($query == '') {
            return response()->json(array('kosong' => $query));
        } else {
            return response()->json(array('status' => true, 'data' => $query));
        }
    }
}
