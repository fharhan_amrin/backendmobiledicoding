<?php

namespace App\Http\Controllers;

use App\Models\MemberLapakUsahaModel;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class MemberLapakUsahaKita extends Controller
{

    public function index()
    {
        $data = DB::table('users')->paginate(8);
        return view('contents/users/view_users_all', ['data' => $data]);
    }

    public function index_detail()
    {

        return view('contents/users/view_users_detail');
    }
}
