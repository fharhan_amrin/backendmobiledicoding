<?php

namespace App\Http\Controllers;

use App\Models\ContentEventModel;
use App\Models\JenisUsahaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {

        $jenisusaha = DB::table('jenis_usaha')->paginate(10);
        return view('contents.dashboard.dashboard_view', ['jenisusaha' => $jenisusaha]);
    }


    public function view_detail_content_one($id)
    {
        $jenisusaha  = JenisUsahaModel::find($id);
        $content = DB::table('contentevent')->where('type_jenis_content', $id)->paginate(10);
        return view('contents.dashboard.dashboard_view_detail_topik', ['content' => $content, 'jenisusaha' => $jenisusaha]);
    }

    public function view_detail_content($id) //id content
    {

        $content  = ContentEventModel::find($id);


        return view('contents.dashboard.dashboard_view_detail', ['content' => $content]);
    }
}
