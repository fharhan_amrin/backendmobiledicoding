<?php

namespace App\Http\Controllers;

use App\Models\NotifikasiKegiatanModel;
use Illuminate\Http\Request;
use DataTables;

class NotifikasiKegiatanController extends Controller
{
    public function index(Request $request, $id)
    {
        if ($request->ajax()) {
            $data = NotifikasiKegiatanModel::select('*')->where('id_user', $id);
            return Datatables::of($data)
                ->addIndexColumn()
                // ->addColumn('name_jenis_usaha', function ($row) {
                //     return $row['name_jenis_usaha'] == '' ? '-' : $row['name_jenis_usaha'];
                // })

                ->addColumn('action', function ($row) {

                    $btn = '<button type="button" onclick="getdataedit(' . $row['id'] . ')" class="btn btn-primary" data-toggle="modal" data-target="#updateactivity">
                              <i style="color:white;" class="fa fa-edit"></i>
                             </button>

                             <button type="button" onclick="deletedata(' . $row['id'] . ')" class="btn btn-danger" >
                              <i style="color:white;" class="fa fa-trash"></i>
                             </button>';

                    return $btn;
                })
                ->rawColumns(['action'])
                // ->removeColumn('id')
                ->make(true);
        }
        return view('contents.activityliburan.activity_view_notifikasi_kegiatan');
    }
    public function store(Request $request)
    {
        $notifikasi = new  NotifikasiKegiatanModel();


        $notifikasi->id_user = $request->id_user;
        $notifikasi->id_activity_kengiatan = $request->id_activity_kengiatan;
        $notifikasi->start_date = $request->start_date;
        $notifikasi->end_date = $request->end_date;
        $notifikasi->per = $request->per;
        $notifikasi->notifikasi_via = $request->notifikasi_via;
        $simpan =  $notifikasi->save();

        if ($simpan) {
            $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
        } else {
            $result = array('status' => false, 'msg' => 'Failed Update Data');
        }


        return response()->json($result);
    }

    public function AllDataNotifikasi()
    {
        $query = NotifikasiKegiatanModel::all();

        if ($query) {
            $result = array('status' => true, 'data' => $query);
        } else {
            $result  = array('status' => false, 'msg' => 'failed get data Sosial Media');
        }

        return response()->json($result);
    }


    public function edit($id)
    {
        $sosialmedia = NotifikasiKegiatanModel::find($id);

        if ($sosialmedia == '') {
            $result = array('status' => false, 'msg' => 'Id kosong', 'data' => $sosialmedia);
        } else {
            if ($sosialmedia) {
                $result = array('status' => true, 'data' => $sosialmedia);
            } else {
                $result = array('status' => false);
            }
        }

        return response()->json($result);
    }

    public function update(Request $request)
    {
        $notifikasi =  NotifikasiKegiatanModel::find($request->id);

        if ($notifikasi == '') {
            $result = array('status' => true, 'msg' => 'id Tidak ada di database');
        } else {
            $notifikasi->id_activity_kengiatan = $request->id_activity_kengiatan;
            $notifikasi->start_date = $request->start_date;
            $notifikasi->end_date = $request->end_date;
            $notifikasi->per = $request->per;
            $notifikasi->notifikasi_via = $request->notifikasi_via;

            $simpan =  $notifikasi->save();

            if ($simpan) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Update Data');
            }
        }

        return response()->json($result);
    }


    public function destroy($id)
    {
        $sosialmedia =  NotifikasiKegiatanModel::find($id);

        if ($sosialmedia == '') {
            $result = array('status' => false, 'msg' => 'id anda yang mau di hapus, tidak tersedia di database');
        } else {
            $delete = $sosialmedia->delete();
            if ($delete) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Delete');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Deletea Data');
            }
        }

        return response()->json($result);
    }
}
