<?php

namespace App\Http\Controllers;

use App\Models\InvoiceModel;
use Illuminate\Http\Request;
use PDF;
use DataTables;

class InvoiceHistoryController extends Controller
{
    public function index(Request $request, $id)
    {
        if ($request->ajax()) {
            $data = InvoiceModel::select('*')->where('id_user', $id);
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('total', function ($row) {
                    return "Rp." . $row['total'];
                })
                ->addColumn('status', function ($row) {
                    return $row['status'] == 0 ? "<span class='label label-warning'>Belum Bayar</span>" : "<span class='label label-success'>Sudah Bayar</span>";
                })
                ->addColumn('action', function ($row) {

                    if ($row['status'] == 0) {
                        $btn = '

           
                    <button type="button" onclick="getdataedit(' . $row['id'] . ')" class="btn btn-info" data-toggle="modal" data-target="#exampleModaledit">
                              Upload Bukti
                             </button>

                             
                             <a href="/generatepdf" class="btn btn-primary">Download pdf</a>

                       
                             ';

                        //        <button type="button" onclick="deletedata(' . $row['id'] . ')" class="btn btn-danger" >
                        //   <i style="color:white;" class="fa fa-trash"></i>
                        //  </button>

                    } else {
                        $btn = '


                             
                             <a href="/generatepdf" class="btn btn-primary">Download pdf</a>

                            

                             ';
                    }



                    return $btn;
                })
                ->rawColumns(['status', 'action'])
                // ->removeColumn('id')
                ->make(true);
        }
        return view('contents.invoice.view_invoice');
    }
    public function store(Request $request)
    {
        $content = InvoiceModel::find($request->id);



        if ($content == '') {
            $result = array('status' => true, 'msg' => 'id Tidak ada di database');
        } else {

            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('image_file_bukti_pembayaran_edit');

            // nama file
            $namefile =  str_replace(" ", "", $file->getClientOriginalName());

            // // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = './uploads/invoice';

            // // upload file
            $file->move($tujuan_upload, str_replace(" ", "", $file->getClientOriginalName()));


            $content->id                        = $request->id;
            $content->image_bukti_pembayaran         = $namefile;
            $content->status         = 1;



            $simpan                                 =  $content->save();

            if ($simpan) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Update Data');
            }
        }


        return response()->json($result);
    }



    public function destroy($id)
    {
        $content =  InvoiceModel::find($id);

        if ($content == '') {
            $result = array('status' => false, 'msg' => 'id anda yang mau di hapus, tidak tersedia di database');
        } else {
            $delete = $content->delete();
            if ($delete) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Delete');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Deletea Data');
            }
        }

        return response()->json($result);
    }

    function generatepdf()
    {
        $pdf = PDF::loadView('contents.invoice.generatepdf');

        return $pdf->download('invoice.pdf');
    }
}
