<?php

namespace App\Http\Controllers;

use App\Models\AlatKegiatanModel;
use Illuminate\Http\Request;
use DataTables;

class AlatKengiantanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = AlatKegiatanModel::select('*');
            return Datatables::of($data)
                ->addIndexColumn()
                // ->addColumn('name_alat', function ($row) {
                //     return $row['name_alat'] == '' ? '-' : $row['name_alat'];
                // })

                ->addColumn('action', function ($row) {

                    $btn = '<button type="button" onclick="getdataedit(' . $row['id'] . ')" class="btn btn-primary" data-toggle="modal" data-target="#updateactivity">
                              <i style="color:white;" class="fa fa-edit"></i>
                             </button>

                             <button type="button" onclick="deletedata(' . $row['id'] . ')" class="btn btn-danger" >
                              <i style="color:white;" class="fa fa-trash"></i>
                             </button>';

                    return $btn;
                })
                ->rawColumns(['action'])
                // ->removeColumn('id')
                ->make(true);
        }
        return view('contents.activityliburan.activity_view_alat_kegiatan');
    }
    public function store(Request $request)
    {
        $alat = new  AlatKegiatanModel();


        $alat->name_alat = $request->name_alat;

        $simpan =  $alat->save();

        if ($simpan) {
            $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
        } else {
            $result = array('status' => false, 'msg' => 'Failed Update Data');
        }


        return response()->json($result);
    }

    public function allDataAlat()
    {
        $query = AlatKegiatanModel::all();

        if ($query) {
            $result = array('status' => true, 'data' => $query);
        } else {
            $result  = array('status' => false, 'msg' => 'failed get data Sosial Media');
        }

        return response()->json($result);
    }


    public function edit($id)
    {
        $alat = AlatKegiatanModel::find($id);

        if ($alat == '') {
            $result = array('status' => false, 'msg' => 'Id kosong', 'data' => $alat);
        } else {
            if ($alat) {
                $result = array('status' => true, 'data' => $alat);
            } else {
                $result = array('status' => false);
            }
        }

        return response()->json($result);
    }

    public function update(Request $request)
    {
        $alat =  AlatKegiatanModel::find($request->id);

        if ($alat == '') {
            $result = array('status' => true, 'msg' => 'id Tidak ada di database');
        } else {
            $alat->name_alat = $request->name_alat;

            $simpan =  $alat->save();

            if ($simpan) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Update Data');
            }
        }

        return response()->json($result);
    }


    public function destroy($id)
    {
        $alat =  AlatKegiatanModel::find($id);

        if ($alat == '') {
            $result = array('status' => false, 'msg' => 'id anda yang mau di hapus, tidak tersedia di database');
        } else {
            $delete = $alat->delete();
            if ($delete) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Delete');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Deletea Data');
            }
        }

        return response()->json($result);
    }
}
