<?php

namespace App\Http\Controllers;

use App\Models\JenisUsahaModel;
use Illuminate\Http\Request;
use DataTables;

class JenisUsahaControler extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = JenisUsahaModel::select('*');
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('name_jenis_usaha', function ($row) {
                    return $row['name_jenis_usaha'] == '' ? '-' : $row['name_jenis_usaha'];
                })
                ->addColumn('created_at', function ($row) {
                    return $row['created_at'];
                })
                ->addColumn('updated_at', function ($row) {
                    return $row['updated_at'];
                })
                ->addColumn('action', function ($row) {

                    $btn = '<button type="button" onclick="getdataedit(' . $row['id'] . ')" class="btn btn-primary" data-toggle="modal" data-target="#updateactivity">
                              <i style="color:white;" class="fa fa-edit"></i>
                             </button>

                             <button type="button" onclick="deletedata(' . $row['id'] . ')" class="btn btn-danger" >
                              <i style="color:white;" class="fa fa-trash"></i>
                             </button>';

                    return $btn;
                })
                ->rawColumns(['action'])
                // ->removeColumn('id')
                ->make(true);
        }
        return view('contents.jenisusaha.view_jenisusaha');
    }
    public function store(Request $request)
    {
        $sosialmedia = new  JenisUsahaModel();


        $sosialmedia->name_jenis_usaha = $request->name_jenis_usaha;
        $sosialmedia->deskripsi        = $request->deskripsi;

        $simpan =  $sosialmedia->save();

        if ($simpan) {
            $result = array('status' => true, 'msg' => 'Data Berhasil Di Update');
        } else {
            $result = array('status' => false, 'msg' => 'Failed Update Data');
        }


        return response()->json($result);
    }

    public function getDatajenisusaha()
    {
        $query = JenisUsahaModel::all();

        if ($query) {
            $result = array('status' => true, 'data' => $query);
        } else {
            $result  = array('status' => false, 'msg' => 'failed get data Sosial Media');
        }

        return response()->json($result);
    }


    public function edit($id)
    {
        $sosialmedia = JenisUsahaModel::find($id);

        if ($sosialmedia == '') {
            $result = array('status' => false, 'msg' => 'Id kosong', 'data' => $sosialmedia);
        } else {
            if ($sosialmedia) {
                $result = array('status' => true, 'data' => $sosialmedia);
            } else {
                $result = array('status' => false);
            }
        }

        return response()->json($result);
    }

    public function update(Request $request)
    {
        $sosialmedia =  JenisUsahaModel::find($request->id);

        if ($sosialmedia == '') {
            $result = array('status' => true, 'msg' => 'id Tidak ada di database');
        } else {
            $sosialmedia->name_jenis_usaha = $request->name_jenis_usaha;
            $sosialmedia->deskripsi          = $request->deskripsi_edit;

            $simpan =  $sosialmedia->save();

            if ($simpan) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Update', 'eidt' => $request->deskripsi_edit);
            } else {
                $result = array('status' => false, 'msg' => 'Failed Update Data');
            }
        }

        return response()->json($result);
    }


    public function destroy($id)
    {
        $sosialmedia =  JenisUsahaModel::find($id);

        if ($sosialmedia == '') {
            $result = array('status' => false, 'msg' => 'id anda yang mau di hapus, tidak tersedia di database');
        } else {
            $delete = $sosialmedia->delete();
            if ($delete) {
                $result = array('status' => true, 'msg' => 'Data Berhasil Di Delete');
            } else {
                $result = array('status' => false, 'msg' => 'Failed Deletea Data');
            }
        }

        return response()->json($result);
    }
}
