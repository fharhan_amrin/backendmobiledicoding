<?php

namespace App\Http\Controllers;

use App\Models\InvoiceModel;
use App\Models\ProductModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DashboardProduct extends Controller
{
    function index()
    {
        $product = DB::table('product')->paginate(8);
        return view('contents.dashboardproduct.dashboardproduct_view', ['product' => $product]);
    }
    function index_detail($id)
    {
        $productdetail = ProductModel::find($id);


        return view('contents.dashboardproduct.dashboardproduct_view_detail', ['productdetail' => $productdetail]);
    }


    function addInvoice(Request $request)
    {

        $invoice = new InvoiceModel();
        $invoice->id_user           = $request->id_user;
        $invoice->id_product        = $request->id_product;
        $invoice->total             = $request->harga_product;
        $invoice->invoice           = "INV" . $request->id_user . date('Ymd');
        $invoice->invoice_start     = date('Y-m-d H:i:s');
        $invoice->invoice_end       = date('Y-m-d H:i:s');
        $invoice->status            = 0; //belum bayar

        $save  =  $invoice->save();

        if ($save) {
            $result = array('status' => true);
        } else {
            $result = array('status' => false);
        }

        return response()->json($result);
    }

    function addInvoice_content(Request $request)
    {

        $invoice = new InvoiceModel();
        $invoice->id_user           = $request->id_user;
        $invoice->id_content        = $request->id_content;
        $invoice->total             = $request->harga_content;
        $invoice->invoice           = "INV" . $request->id_user . date('Ymd');
        $invoice->invoice_start     = date('Y-m-d H:i:s');
        $invoice->invoice_end       = date('Y-m-d H:i:s');
        $invoice->status            = 0; //belum bayar

        $save  =  $invoice->save();

        if ($save) {
            $result = array('status' => true);
        } else {
            $result = array('status' => false);
        }

        return response()->json($result);
    }
}
