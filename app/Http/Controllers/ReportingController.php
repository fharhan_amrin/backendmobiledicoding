<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportingController extends Controller
{
    public function index()
    {
        return view('contents.reporting.view_reporting');
    }
    public function detailed()
    {
        return view('contents.reporting.view_reporting_detailed');
    }
}
