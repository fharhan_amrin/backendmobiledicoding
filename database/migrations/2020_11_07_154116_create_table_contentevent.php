<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableContentevent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contentevent', function (Blueprint $table) {
            $table->id();
            $table->string('id_user_pembeli')->nullable();
            $table->string('id_user_pembuat')->nullable();
            $table->string('type_jenis_content')->nullable();
            $table->string('judul_event')->nullable();
            $table->text('deskripsi_event')->nullable();
            $table->string('image_event')->nullable();
            $table->string('link_vidio_penjelasan_event')->nullable(); //youtube dll
            $table->string('link_vidio_free_hiburan')->nullable(); //youtube dll
            $table->string('link_event')->nullable(); //zoom,google meet dll
            $table->string('harga')->nullable();
            $table->string('diskon')->nullable();
            $table->string('status_content')->nullable(); // 1release, 2 pending, 3 tutup
            $table->string('kapasitas_penonton')->nullable(); //jumlahpenonton
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contentevent');
    }
}
