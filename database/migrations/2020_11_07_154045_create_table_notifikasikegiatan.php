<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableNotifikasikegiatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifikasikegiatan', function (Blueprint $table) {
            $table->id();
            $table->string('id_user');
            $table->string('id_activity_kengiatan')->nullable(); //terkirim/tidak
            $table->datetime('start_date', 0)->nullable();
            $table->datetime('end_date', 0)->nullable();
            $table->string('per')->nullable();
            $table->string('status_pesan')->nullable(); //terkirim/tidak
            $table->string('notifikasi_via')->nullable(); //email,telegram
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifikasikegiatan');
    }
}
