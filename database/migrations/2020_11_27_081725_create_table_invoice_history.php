<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableInvoiceHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_history', function (Blueprint $table) {
            $table->id();
            $table->string('id_user');
            $table->string('image_bukti_pembayaran')->nullable();
            $table->string('total')->nullable();
            $table->string('id_content')->nullable(); //content event
            $table->string('id_product')->nullable(); //content product
            $table->string('invoice')->nullable();
            $table->string('invoice_start')->nullable(); //awal pembayaran
            $table->string('invoice_end')->nullable(); //batas pembayaran->nullable()
            $table->string('status')->nullable(); // 0 belum bayar 1 sudah bayar
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_history');
    }
}
