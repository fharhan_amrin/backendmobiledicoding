<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableActivityliburan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activityliburan', function (Blueprint $table) {
            $table->id();
            $table->string('iduser')->nullable();
            $table->string('to')->nullable();
            $table->string('berapa_hari')->nullable();
            $table->string('type_of_trip')->nullable();
            $table->string('id_name_rencana_kegiatan')->nullable();
            $table->string('id_alatkegiatan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activityliburan');
    }
}
