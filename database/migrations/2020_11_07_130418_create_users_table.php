<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('notelepon')->nullable();
            $table->string('umur')->nullable();
            $table->string('alamat')->nullable();
            $table->string('email')->nullable();
            $table->string('foto_profile')->nullable();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->string('status_user')->nullable(); //aktif/tidak aktif
            $table->string('level_user')->nullable(); //admin dll
            $table->string('pekerjaan')->nullable();
            $table->string('id_jenis_usaha')->nullable();
            $table->string('name_jenis_usaha')->nullable();
            $table->string('kelompok_usaha')->nullable();
            $table->string('line')->nullable();
            $table->string('twiter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('tiktok')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
