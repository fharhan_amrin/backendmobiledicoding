<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableKelompokProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string('id_user')->nullable();
            $table->string('image_product')->nullable();
            $table->string('name_product')->nullable();
            $table->string('harga_product')->nullable();
            $table->string('stock_product')->nullable();
            $table->string('status_product')->nullable(); //0==tersedia,1=habis ,2= sedang di perjalanan
            $table->string('id_jenis_usaha')->nullable();
            $table->string('name_jenis_usaha')->nullable();
            $table->string('deskripsi_penjelasan_product')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
