<!DOCTYPE html>
<html style="background: #eff1f2;">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
         <meta name="dicoding:email" content="fharhanamrin028@gmail.com">

         <link rel="icon" type="image/png" href="{{ url('')}}/satu.png">

        <title>@yield('title','LapakUsahaKita.com')</title>

        <link href="{{url('')}}/dashboard/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/dashboard/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/dashboard/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="{{url('')}}/dashboard/assets/js/modernizr.min.js"></script>

        
        {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> --}}
    <link rel="stylesheet" href="{{url('')}}/dataTables.bootstrap4.min.css">
    <script src="{{url('')}}/jquery-3.5.1.js"></script>

              {{-- datatable --}}


    <script src="{{url('')}}/jquery.dataTables.min.js"></script>
    <script src="{{url('')}}/dataTables.bootstrap4.min.js"></script>

   

        {{-- akhir datatable --}}

        {{-- sweet alert --}}
    <link rel="stylesheet" href="{{url('')}}/dist/sweetalert2.css">
    <script src="{{url('')}}/dist/sweetalert2.all.min.js"></script>

    {{-- highchart --}}

    {{-- <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script> --}}

    {{-- akhir highchart --}}

    {{-- select bootstrap --}}
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>


    <style>
        .navbar-custom{
            background:linear-gradient(to right, #1565C0, #b92b27) !important;
        }
        .topbar-left{
            background:linear-gradient(to right, #1565C0, #b92b27) !important;

        }

        .button-menu-mobile{
            background:linear-gradient(to right, #b92b27, #ff443d) !important;

        }
        .fa-info-circle{
            color:linear-gradient(to right, #1565C0, #b92b27) !important;
        }

        @font-face {
        font-family: myFirstFont;
        src: {{url('roboto-slab/RobotoSlab-Light.ttf')}};
        }
        *{
          font-family: myFirstFont;
          color: black;
        }
          
        h1{
          font-family: myFirstFont;

        }

        span{
            color: black;
        }
        ul li a{
            color:black !important;
        }

        ul li span{
            color:black !important;

        }

        a .active  span{
            color: black !important;
        }
        #sidebar-menu > ul > li > a.active {
            background:#bd0000 !important;
            border-left: 3px solid #2561b7 !important;
            color: black !important;
        }

         #sidebar-menu > ul > li >  {
            /* background: white !important; */
            border-left: 3px solid #2561b7 !important;
            /* color: white !important; */
        }
        .ti-files{
            color: white;
        }

        .page-link{
            color: black !important;
        }
    </style>


    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                    <a href="{{url('/bukausaha')}}" class="logo"><i class="icon-magnet icon-c-logo"> </i><span  style="color: white;">LUK</span></a>
                        <!-- Image Logo here -->
                        <!--<a href="index.html" class="logo">-->
                        <!--<i class="icon-c-logo"> <img src="{{url('')}}/dashboard/assets/images/logo_sm.png" height="42"/> </i>-->
                        <!--<span><img src="{{url('')}}/dashboard/assets/images/logo_light.png" height="20"/></span>-->
                        <!--</a>-->
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <nav class="navbar-custom">

                    <ul class="list-inline float-right mb-0">
                       

                        <li class="list-inline-item notification-list">
                            <a class="nav-link waves-light waves-effect" href="#" id="btn-fullscreen">
                                <i class="dripicons-expand noti-icon"></i>
                            </a>
                        </li>

                        <li class="list-inline-item notification-list">
                            <a class="nav-link right-bar-toggle waves-light waves-effect" href="#">
                                <i class="dripicons-message noti-icon"></i>
                            </a>
                        </li>

                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="{{url('')}}/dashboard/assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Welcome ! John</small> </h5>
                                </div>

                                <!-- item-->
                            
                                <!-- item-->
                            {{-- <a href="{{route('profileuser')}}" class="dropdown-item notify-item" style="color: black !important;" >
                                    <i class="md md-account-circle"></i> <span style="color: black !important;" >Profile</span>
                                </a> --}}

                               
                                <!-- item-->
                            <a href="{{route('registeruser')}}" onclick="Logout()" class="dropdown-item notify-item" style="color: black !important;" >
                                    <i class="md md-settings-power"></i> <span style="color: black !important;" >Logout</span>
                                </a>

                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left waves-light waves-effect">
                                <i class="dripicons-menu"></i>
                            </button>
                        </li>
                       
                    </ul>

                </nav>

            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->

            <div class="left side-menu" style="background: linear-gradient(to right, #1565C0, #b92b27) !important">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu" style="background: linear-gradient(to right, #1565C0, #b92b27) !important">
                        <ul>

                                 <li class="text-muted menu-title" style="color: white !important;">Manage Content</li>

                            <li class="has_sub">
                                <a href="{{url('')}}/bukausaha" class="waves-effect"><i class="ti-files"></i> <span> Dashboard Content </span></a>
                                
                            </li>
                                     <li class="has_sub">
                                <a href="{{url('')}}/bukausahaproduct" class="waves-effect"><i class="ti-files"></i> <span> Dashboard Product </span></a>
                                
                            </li>

                           

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i> <span> Manage Content </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                <li id="managecontent"></li>
                                <li id="type_jenis_content"><a href="{{url('jenisusaha')}}">Type Jenis Content</a></li>
                                  <li id="kelompok_usaha"><a href="{{url('/kelompokusaha')}}">Kelompok Usaha</a></li>
                                </ul>
                            </li>

                             <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i> <span> Manage Product </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                  <li id="manageproduct"></li>
                                </ul>
                            </li>
                            

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i> <span> Manage Liburan </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled" id="manageliburan-menu">
                                  
                                </ul>
                            </li>

          

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i> <span> Manage Reporting </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled" id="managereporting-menu">
                                
                                </ul>
                            </li>

                           


                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i> <span> Akun </span> <span class="menu-arrow"></span></a>

                                <ul class="list-unstyled" id="manageakun">
                                   
                                </ul>
                                
                            </li>

                                       <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i> <span> Member </span> <span class="menu-arrow"></span></a>

                                <ul class="list-unstyled">
                                    <li><a href="{{url('/member')}}">All Member</a></li>
                                
                                </ul>
                                
                            </li>



                            

                           

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page" style="background: #eff1f2;">
                <!-- Start content -->
                <div class="content" style="background: #eff1f2;">
                    <div class="container-fluid">

                        


                      @yield('content')



                      


                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right" style="background: linear-gradient(to right, #1565C0, #b92b27) !important;">
                    &copy; 2016 - 2018. All rights reserved.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        {{-- <script src="{{url('')}}/dashboard/assets/js/jquery.min.js"></script> --}}
        <script src="{{url('')}}/dashboard/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
        <script src="{{url('')}}/dashboard/assets/js/bootstrap.min.js"></script>
        <script src="{{url('')}}/dashboard/assets/js/detect.js"></script>
        <script src="{{url('')}}/dashboard/assets/js/fastclick.js"></script>
        <script src="{{url('')}}/dashboard/assets/js/jquery.slimscroll.js"></script>
        <script src="{{url('')}}/dashboard/assets/js/jquery.blockUI.js"></script>
        <script src="{{url('')}}/dashboard/assets/js/waves.js"></script>
        <script src="{{url('')}}/dashboard/assets/js/wow.min.js"></script>
        <script src="{{url('')}}/dashboard/assets/js/jquery.nicescroll.js"></script>
        <script src="{{url('')}}/dashboard/assets/js/jquery.scrollTo.min.js"></script>

        <script src="{{url('')}}/dashboard/plugins/peity/jquery.peity.min.js"></script>

        <script src="{{url('')}}/dashboard/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>

        <script src="{{url('')}}/dashboard/assets/pages/jquery.dashboard_3.js"></script>

        <script src="{{url('')}}/dashboard/assets/js/jquery.core.js"></script>
        <script src="{{url('')}}/dashboard/assets/js/jquery.app.js"></script>
        <script>
            $(document).ready(function () {
               console.log(localStorage.getItem('username'));
               let id = localStorage.getItem('id');

               let level_user = localStorage.getItem('level_user'); //0=admin
                // menu untuk admin
               if (level_user ==0) {

                   $('#type_jenis_content').empty();
                   $('#kelompok_usaha').empty();

                   $('#type_jenis_content').append(`
                     <a href="{{url('jenisusaha')}}">Type Jenis Content</a>
                   `);

                    $('#kelompok_usaha').append(`
                     <a href="{{url('/kelompokusaha')}}">Kelompok Usaha</a>
                   `);
                   
               }


               $('#managecontent').empty();
               $('#manageproduct').empty();
               $('#manageliburan-menu').empty();
               $('#managereporting-menu').empty();
               $('#manageakun').empty();

               $('#managecontent').append(
                   `<a href="{{url('managecontent/${id}')}}">Manage Content</a>`
                   )
                $('#manageproduct').append(`<a href="{{url('/product/${id}')}}">Product</a>`);  
                $('#manageliburan-menu').append(`
                 <li><a href="{{url('/rencana/activityliburan/${id}')}}">Aktivity Liburan</a></li>
                <li><a href="{{url('/rencana/rencanakegiatan')}}">Aktivity Yang Di Rencenakan</a></li>
                <li><a href="{{url('/rencana/alatkegiatan')}}">Manage Alat-Alat Liburan</a></li>
                <li><a href="{{url('rencana/notifikasikegiatan/${id}')}}">Notifikasi Liburan</a></li>
                `);  
                $('#managereporting-menu').append(
                    `<li><a href="{{url('reporting')}}">Summary Report</a></li>
                                <li><a href="{{url('detailed')}}">Detailed Report</a></li>`
                    );
                $('#manageakun').append(
                    `
                                <li><a href="{{url('invoice/${id}')}}">Invoice History</a></li>`
                );    
            });


            function Logout() {

                localStorage.clear();

                setTimeout(() => {
                    window.location.href = "{{url('/registeruser')}}"
                }, 1000);
                
            }
          
        </script>
    </body>
</html>