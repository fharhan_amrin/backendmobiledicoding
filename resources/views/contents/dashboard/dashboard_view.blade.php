@extends('welcome')

@section('title')
    LapakUsahaKita.com
@endsection

@section('content')

<style>
    svg{
        width: 10px !important;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <h2 class="text-center">Selamat Datang di <span  style="color:#8b3a56">LapakUsahaKita.com</span> </h2>
              
            @foreach($jenisusaha as $p)
              <div class="card-body">
              <div class="card-title"><h5>{{$p->name_jenis_usaha}}</h5></div>
               <div  style="overflow: scroll;display: flex;" id="data-{{$p->id}}"  >
               </div>
            </div>
            
            @endforeach
        </div>
        {{-- pagination --}}

        <br>

        <div class="d-flex justify-content-center">
            {!! $jenisusaha->links() !!}
        </div>


    </div>

    
</div>


<script>
    $(document).ready(function () {
        getDataContent();
    });


    

    function getDataContent() {

       $.ajax({
           type: "get",
           url: `{{url('/api/content/allDataContent')}}`,
           dataType: "JSON",
           success: function (response) {

               for (let x = 1; x <= 20; x++) {

                    $('#data-'+x).append(`
                       <div class="col-md-3">
                          
                       <div class="card">
                            
                       <div class="card-body" style="height: 349px;">
                          <span style="position: relative;top: 50%;left: 34%;"> <a href="{{url('/bukausaha_detail_topik/')}}/${x}">View All </a></span>
                       </div>
                   </div>
                   </div>
                   `);
                    
                     for (let index = 0; index < response.data.length; index++) {
                        if (response.data[index].type_jenis_content == x ) {
                     $('#data-'+x).append(`
                      <div class="col-md-3" >
                       <div class="card">
                    
                       <img class="card-img-top" style="    width: 100%;
    height: 146px;" src="{{url('')}}/uploads/${response.data[index].image_event}" alt="Card image cap">
                             
                       <div class="card-body" style="height: 200px;">
                          <h6> ${response.data[index].judul_event}</h6>
                          <span style="display:block;">Kapasitas Penonton : ${response.data[index].kapasitas_penonton}</span>
                          <span style="display:block;"> Created At :  ${formatDate(response.data[index].created_at)}</span>
                          <span style="display:block;"> Updated At : ${formatDate(response.data[index].updated_at)}</span>
                          
                           
                           
                       </div>
                   </div>
                   </div>

                    
                   
                   `);
                }
                     }


                    
                   
               }
            
           }
       });

 
        
    }

    function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
</script>
    
@endsection