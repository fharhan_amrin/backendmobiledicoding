@extends('welcome')

@section('title')
    LapakUsahaKita.com
@endsection

@section('content')

<style>

     svg{
        width: 10px !important;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <h2 class="text-center">  Topik <span style="color:orange;">#{{$jenisusaha->name_jenis_usaha}}</span></h2>
              
            <div class="card-body">
            <div class="card-title"><h5>{{$jenisusaha->name_jenis_usaha}}</h5></div>

                {{-- awal card --}}
                <div class="card" style=" margin: 1%;">
                    <div class="card-body">
                        <div class="row">
                            @foreach ($content as $item)
                            <div class="col-md-3">
                            <img src="{{url('')}}/uploads/{{$item->image_event}}" style="width: 84%;" alt="">
                            </div>
                            <div class="col-md-9">
                            <h3>{{$item->judul_event}}</h3>
                            <p>{{$item->deskripsi_event}}</p>
                                <div class="row">
                                    <div class="col-md-2">
                                        <span style="font-size: 10px;font-weight: bold; display:block;"> Status Content  </span>
                                    </div>

                                    <div class="col-md-4">
                                        <span style="font-size: 10px;font-weight: bold; display:block;">  : 
                                         @if ($item->status_content == 1)
                                            Release
                                         @elseif ($item->status_content == 2)
                                                Pending
                                         @else
                                            Tutup
                                         @endif
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1">
                                    <a href="{{url('bukausaha_detail/'.$item->id)}}" class="btn btn-primary">Detail</a>
                                    </div>
                                   
                                    
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                {{-- akhir card --}}

               

                
                <div class="d-flex justify-content-center">
                    
                    {{ $content->links() }}
                </div>
              
              

                 
               
            </div>
        </div>
       
    </div>

    
</div>
    
@endsection