@extends('welcome')

@section('title')
    LapakUsahaKita.com
@endsection

@section('content')



<div class="row mt-1">
    <div class="col-md-12">
        <div class="card">
            <img src="{{asset('uploads/product/'.$content->image_event)}}" alt="image" class="card-img-top" alt="...">
            <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                      <h4>Link Vidio Penjelasan Event</h4>
                     <iframe width="420" height="345" src="{{$content->link_vidio_free_hiburan}}">
             </iframe>
                </div>
                <div class="col-md-6">
                      <h4>Link Vidio free Hiburan</h4>

                     <iframe width="420" height="345" src="{{$content->link_vidio_free_hiburan}}">
             </iframe>
                </div>
            </div>
           
             <h2 style="color: black;"> {{$content->judul_event}}</h2>
            <h6 style="color: grey;"> Rp. {{$content->harga}}</h6>
            <h6 style="color: grey;"> {{$content->kapasitas_penonton}} Penonton</h6>
            <h5 class="font-600">Description Event:</h5>
          <p>{{$content->deskripsi_event}}</p>
            <h6 style="color: grey;">Created At {{$content->created_at}}</h6>
            <h6 style="color: grey;"> Updated At{{$content->updated_at}}</h6>

        <input type="hidden" id="type_jenis_content" name="type_jenis_content" value="{{$content->type_jenis_content}}">


            <?php 
               if ($content->harga =='' or $content->harga == 0) {
            ?>
              <span style="display: block">Link Event</span>
               <a href="{{$content->link_event}}">Click Menuju link</a>
               <h2>Selamat Mengikuti Event.</h2>
            <?php } else{?>
           <button class="btn btn-primary" onclick="addInvoice({{$content->id}},{{$content->harga}})" > <b>Ikut Rp. {{$content->harga}} </b></button>
               <?php }?>
            </div>
        </div>
    </div>
</div>


<h4><b>Disarankan</b></h4>

<div class="row mt-1">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body" style="overflow: scroll;">
                <div style="display: flex;" id="data-detail">
                    
                    
                    
                    
                </div>
            </div>
        </div>
    </div>

</div>


<script>
    $(document).ready(function () {
       load_data_topik(`{{$content->type_jenis_content}}`);
    });

    

     function addInvoice(id,harga) {
         console.log(harga);
        let id_content  = id;
        let harga_content  = harga;
        let id_user  = localStorage.getItem('id');
         Swal.fire({
        title: 'Apakah kamu yakin?',
        text: "Data Ini akan Di masukan langsung ke invoice!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, to invoice'
        }).then((result) => {
        if (result.isConfirmed) {
    
            $.ajax({
                type: "POST",
                url: `{{url('api/addInvoice_content')}}`,
                data: {
                    id_user:id_user,
                    id_content:id_content,
                    harga_content:harga_content
                },
                dataType: "JSON",
                success: function (response) {
                    if (response.status) {
                        Swal.fire(
        'Berhasil!',
         `<span style="display:block;">Silahkan Check Di menu Invoice History anda </span> <span style="display:block;">Segera Lunaskan ya...</span><span style="display:block;"> jangan sampe kehabisan !</span>`,
        'success'
        )
                    }
                }
            });
        }
        })
    }


    function load_data_topik(content) { //id content

        $('#data-detail').append(
            `<div class="col-md-3">
                          
                       <div class="card">
                            
                       <div class="card-body" style="height: 349px;">
                          <span style="position: relative;top: 50%;left: 34%;"> <a href="{{url('/bukausaha_detail_topik/')}}/${content}">View All </a></span>
                       </div>
                   </div>
                   </div>`
        )

       $.ajax({
           type: "get",
           url: "{{url('/api/contentTopik/')}}/"+content,
           dataType: "JSON",
           success: function (response) {
               response.data.forEach(function(item) {
                   $('#data-detail').append(
                       ` <div class="col-md-3" >
                       <div class="card">
                    
                       <img class="card-img-top" style="    width: 100%;
    height: 146px;" src="{{url('')}}/uploads/${item.image_event}" alt="Card image cap">
                             
                       <div class="card-body" style="height: 200px;">
                          <h6> ${item.judul_event}</h6>
                          <span style="display:block;">Kapasitas Penonton : ${item.kapasitas_penonton}</span>
                          <span style="display:block;"> Created At :  ${formatDate(item.created_at)}</span>
                          <span style="display:block;"> Updated At : ${formatDate(item.updated_at)}</span>
                          
                           
                           
                       </div>
                   </div>
                   </div>`
                   )
               })
           }
       });
       
        

    }

    function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

</script>




@endsection

