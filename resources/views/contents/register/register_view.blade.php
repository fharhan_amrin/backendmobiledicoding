
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		 <meta name="dicoding:email" content="fharhanamrin028@gmail.com">

		  <link rel="icon" type="image/png" href="{{ url('')}}/satu.png">

		<title>LapakUsahaKita.com</title>

		<link href="{{url('')}}/dashboard/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="{{url('')}}/dashboard/assets/css/icons.css" rel="stylesheet" type="text/css" />
		<link href="{{url('')}}/dashboard/assets/css/style.css" rel="stylesheet" type="text/css" />

		{{-- <script src="{{url('')}}/dashboard/assets/js/modernizr.min.js"></script> --}}
		
		  {{-- sweet alert --}}
    <link rel="stylesheet" href="{{url('')}}/dist/sweetalert2.css">
	<script src="{{url('')}}/dist/sweetalert2.all.min.js"></script>
	
	<style>
		 @font-face {
        font-family: myFirstFont;
        src: {{url('roboto-slab/RobotoSlab-Light.ttf')}};
        }
        *{
          font-family: myFirstFont;
        }
	</style>

	</head>
	<body>

		<div class="account-pages" style="background: linear-gradient(to right, #1565C0, #b92b27) !important;"></div>
		<div class="clearfix"></div>
		
		
		<div class="container-alt">
			<div class="row">
			<div class="col-sm-10 offset-sm-1">
				<div class="wrapper-page signup-signin-page">
					<div class="card-box">
						<div class="panel-heading">
							<h4 class="text-center"> Selamat datang di <strong class="text-custom" style="color:#6d4775 !important">LapakUsahaKita.com</strong></h4>
						</div>
		
						<div class="p-20">
							<div class="row">
								<div class="col-lg-12">
                        <ul class="nav nav-tabs tabs">
                            <li class="nav-item tab">
                                <a href="#home-2" data-toggle="tab" aria-expanded="false" class="nav-link active show">
                                    Sign In
                                </a>
                            </li>
                            <li class="nav-item tab">
                                <a href="#register" data-toggle="tab" aria-expanded="true" class="nav-link">
                                    Sign Up
                                </a>
                            </li>
                            
                        </ul>
                        
                        <div class="tab-content">
                            <div class="tab-pane active" id="home-2">
								<form action="javascript:void(0)" id="formLogin" method="post">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="username">Username : <span style="color:red;"> *</span></label>
										<input type="text" name="username" id="username" class="form-control">
									</div>
									<div class="form-group">
										<label for="password">password : <span style="color:red;"> *</span></label>
										<input type="password" name="password" id="password" class="form-control">
									</div>
									

									<button class="btn btn-danger">Cancel</button>
									<button type="submit" class="btn btn-primary">Login</button>
								</div>
							</div>
						</form>
                            </div>
                            <div class="tab-pane" id="register">
								<form action="javascript:void(0)" id="formRegister" method="post" enctype="multipart/form-data">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="nama">Nama : <span style="color:red;"> *</span></label>
												<input type="text" name="nama" class="form-control" id="nama">
											</div>
										</div>
											<div class="col-md-6">
											<div class="form-group">
												<label for="umur">Umur : <span style="color:red;"> *</span></label>
												<input type="text" name="umur" id="umur" class="form-control">
											</div>
										</div>

											<div class="col-md-6">
											<div class="form-group">
												<label for="jeniskelamin">Jenis Kelamin : <span style="color:red;"> *</span></label>
												<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
													<option value="">Not Selected</option>
													<option value="Perempuan">Perempuan</option>
													<option value="Laki-Laki">Laki-laki</option>
													</select>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label for="No.Telepon">No.Telepon : <span style="color:red;"> *</span></label>
												<input type="text" name="notelepon" id="notelepon" class="form-control"  placeholder="+62">
											</div>
										</div>
										

										<div class="col-md-6">
											<div class="form-group">
												<label for="Email">Email : <span style="color:red;"> *</span></label>
												<input type="text" name="email" class="form-control" id="email">
											</div>
											<div class="form-group">
												<label for="">username</label>
												<input type="text" class="form-control" id="username_i" name="username">
											</div>
											<div class="form-group">
												<label for="">Password</label>
												<input type="password" class="form-control" id="password_i" name="password">
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<label for="foto">foto : <span style="color:red;"> *</span></label>
												<input type="file" name="foto_profil" id="foto_profil" class="form-control">
											</div>

											<div class="form-group">
												<label for="foto">Pekerjaan : <span style="color:red;"> *</span></label>
												<input type="text" name="pekerjaan" id="pekerjaan" class="form-control">
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label for="jenisusaha">Jenis Usaha :</label>
												<select name="id_jenis_usaha" id="id_jenis_usaha" class="form-control">
													
													
												</select>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label for="kelompok_usaha">Kelompok Usaha Jenis Content :</label>
												<select name="kelompok_usaha" id="kelompok_usaha" class="form-control">
													
													<option value="">Not Selected</option>
													<option value="2">Hiburan</option>
													<option value="3">Penjualan</option>
												</select>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label for="alamat">Alamat : <span style="color:red;"> *</span></label>
												<textarea name="alamat" id="alamat" class="form-control" cols="20" rows="5"></textarea>
											</div>
										</div>

										

							
										
										

									</div>
									{{-- for sosial media --}}
									<h2>Sosial Media</h2>
									<div class="row mt-1">
										<div class="col-md-6">
											<div class="form-group">
												<label for="">Twiter</label>
												<input type="text" name="twiter" id="twiter" class="form-control" placeholder="Enter link profil twiter anda">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="">Instagram</label>
												<input type="text" name="instagram" id="instagram" class="form-control" placeholder="Enter link profil instagram">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="">Line</label>
												<input type="text" name="line" id="line" class="form-control" placeholder="Enter link profile line anda">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="">Facebook</label>
												<input type="text" name="facebook" id="facebook" class="form-control" placeholder="Enter link profil facebook anda">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="">Tiktok</label>
												<input type="text" name="tiktok" id="tiktok" class="form-control" placeholder="Enter link profil tiktok anda">
											</div>
										</div>
									</div>

									<div class="clearfix">
										<input type="button" class="btn btn-danger" value="Cancel">
										<input type="submit" class="btn btn-primary" value="Save">
									</div>
							   </form>
                            </div>
                           
                        </div>
                    </div>
								
								
								
								
								
							</div>
							
						</div>
					</div>
		
				</div>
			</div>
		</div>
		</div>


		<!-- jQuery  -->
		<script src="{{url('')}}/jquery-3.5.1.js"></script>
		<script src="{{url('')}}/dashboard/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
		<script src="{{url('')}}/dashboard/assets/js/bootstrap.min.js"></script>
		<script src="{{url('')}}/dashboard/assets/js/waves.js"></script>
		<script src="{{url('')}}/dashboard/assets/js/jquery.slimscroll.js"></script>
		<script src="{{url('')}}/dashboard/assets/js/jquery.scrollTo.min.js"></script>

		<!-- App js -->
		<script src="{{url('')}}/dashboard/assets/js/jquery.core.js"></script>
		{{-- <script src="{{url('')}}/dashboard/assets/js/jquery.app.js"></script> --}}

		<script>
			$(document).ready(function () {
				console.log("ini cetak di halaman register"+localStorage.getItem('username'));
				formLogin();
				formRegister();
				type_jenis_content();
			});

			function formLogin() {
				$('#formLogin').submit(function(e) {
					e.preventDefault();
					$.ajax({
					type: "POST",
					url: "{{url('api/user/login')}}",
					data: $('#formLogin').serialize(),
					dataType: "json",
					success: function (response) {
						if (response.status) {
							let {id,email,username,status_user,level_user} = response.data[0];
							localStorage.setItem('id',id);
							localStorage.setItem('email',email);
							localStorage.setItem('username',username);
							localStorage.setItem('status_user',status_user);
							localStorage.setItem('level_user',level_user);

							// console.log(response.data[0]);

							Swal.fire(
								'Login Success!',
								'You clicked the button!',
								'success'
								)


							setTimeout(() => {	
							window.location.href= "{{url('/bukausaha')}}"
							}, 3000);
						}else{
							console.log("form login"+response);
						}
					 }
				  });
				})			
		    }

			function formRegister() {
				$('#formRegister').submit(function (e) {
					e.preventDefault();
					let nama  					= $('#nama').val();
					let jenis_kelamin  			= $('#jenis_kelamin').val();
					let umur  					= $('#umur').val();
					let notelepon  					= $('#notelepon').val();
					let alamat  				= $('#alamat').val();
					let email  					= $('#email').val();
					let username  				= $('#username_i').val();
					let password  				= $('#password_i').val();
					let pekerjaan  				= $('#pekerjaan').val();
					let id_jenis_usaha  		= $('#id_jenis_usaha').val();
					let name_jenis_usaha  		= $('#name_jenis_usaha').val();
					let kelompok_usaha  		= $('#kelompok_usaha').val();
					let line  					= $('#line').val();
					let twiter  				= $('#twiter').val();
					let instagram  				= $('#instagram').val();
					let facebook  				= $('#facebook').val();
					let tiktok  				= $('#tiktok').val();
					var file_data 				= $('#foto_profil').prop('files')[0];   
					var form_data 				= new FormData();                  
					form_data.append('foto_profil', file_data);
					form_data.append('nama',nama);
					form_data.append('jenis_kelamin',jenis_kelamin);
					form_data.append('umur',umur);
					form_data.append('notelepon',notelepon);
					form_data.append('alamat',alamat);
					form_data.append('email',email);
					form_data.append('username',username);
					form_data.append('password',password);
					form_data.append('pekerjaan',pekerjaan);
					form_data.append('id_jenis_usaha',id_jenis_usaha);
					form_data.append('name_jenis_usaha',name_jenis_usaha);
					form_data.append('kelompok_usaha',kelompok_usaha);
					form_data.append('line',line);
					form_data.append('twiter',twiter);
					form_data.append('instagram',instagram);
					form_data.append('facebook',facebook);
					form_data.append('tiktok',tiktok);
					$.ajax({
					type: "POST",
					url: `{{url('/api/user/create')}}`,
					dataType: 'JSON',  // what to expect back from the PHP script, if anything
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					success: function (response) {
						if (response.status) {
							Swal.fire(
							'Berhasil !',
							'Silahkan Langung Login!',
							'success'
							)
						}else{
							console.log("ini value untuk create"+response)
						}
					}
				});
				})				
			}

			function type_jenis_content() {
      $('#id_jenis_usaha').empty();
      $('#id_jenis_usaha_edit').empty();
	  $('#id_jenis_usaha').append(`
	  <option value="">Tidak ada Usaha</option>`);
      $.ajax({
        type: "get",
        url: `{{url('/api/jenisusaha/getDatajenisusaha')}}`, data: "data",
        dataType: "JSON",
        success: function (response) {
          let {status,data} = response
          if (status) {
             data.forEach(function(item) {
               $('#id_jenis_usaha').append(
                 `<option value="${item.id}">${item.name_jenis_usaha}</option>`
               )

               $('#id_jenis_usaha_edit').append(
                 `<option value="${item.id}">${item.name_jenis_usaha}</option>`
               )
             });
          }
        }
      });


    }
		</script>

	</body>
</html>