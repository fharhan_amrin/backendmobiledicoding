@extends('welcome')

@section('title')
   LapakUsahaKita.com - Member
@endsection

@section('content')

<style>
    svg{
        width: 10px !important;
    }
</style>

<div class="container">
    <h3> Users LapakUsahaKita.com</h3>

   

                        <!-- SECTION FILTER
                        ================================================== -->
                        {{-- <div class="row m-t-10 m-b-10">

                            <div class="col-lg-6">
                                <form role="form">
                                    <div class="form-group contact-search m-b-0">
                                        <input type="text" id="search" class="form-control product-search" placeholder="Search here...">
                                        <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                                    </div> <!-- form-group -->
                                </form>
                            </div>

                            <div class="col-lg-6">
                                <div class="h5 m-0 text-right">
                                    <label class="vertical-middle m-r-10">Sort By:</label>
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-secondary active">
                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Status
                                        </label>
                                        <label class="btn btn-secondary">
                                            <input type="radio" name="options" id="option2" autocomplete="off"> Type
                                        </label>
                                        <label class="btn btn-secondary">
                                            <input type="radio" name="options" id="option3" autocomplete="off"> Name
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div> --}}

                        {{-- end section filter --}}
    <div class="row">

        @foreach ($data as $item)
            
       
        <div class="col-sm-6 col-lg-4">
                    <div class="card-box" style="    height: 236px;">
                        <div class="contact-card">
                            <a class="pull-left" href="#">
                            <img class="rounded" src="{{ asset('/uploads/fotouser/'.$item->foto_profile)}}" alt="user">
                            </a>
                            <div class="member-info">
                                <h4 class="m-t-0 m-b-5 header-title"><b>{{$item->nama}}</b></h4>
                            <p class="text-muted">{{$item->pekerjaan}}</p>
                            <p class="text-dark"><i class="md md-phone m-r-10"></i><small>{{$item->notelepon}}</small></p>
                                
                            </div>

                            <ul class="social-links list-inline m-0">
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="{{$item->facebook}}" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="{{$item->twiter}}" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                </li>
                               
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="{{$item->line}}" data-original-title="Line"><i class="fa fa-line">Line</i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="{{$item->instagram}}" data-original-title="Instagram"><i class="fa fa-instagram"></i></a>
                                </li>

                                 <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="{{$item->tiktok}}" data-original-title="Tiktok"><i class="fa fa-tiktok">Tiktok</i></a>
                                </li>
                            </ul>

                        </div>
                        <a href="{{url('/member/detail/1')}}" class="btn btn-primary mt-4" style="width: 100%;">Detail</a>
                    </div>

       </div> <!-- end col -->
        @endforeach
    </div>

     <div class="d-flex justify-content-center">
            {!! $data->links() !!}
        </div>
</div>

@endsection
