@extends('welcome')

@section('title')
    LapakUsahaKita.com
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                       <img class="rounded" src="{{ asset('/uploads/fotouser/user.jpg')}}" alt="user">
                    </div>

                    <div class="col-md-9">
                        Masih Tahan Pengembangan aplikasi!
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
          <div class="card">
             <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist" style="width: 100%;">
                        <li class="nav-item" role="presentation" style="width: 50%;">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Content</a>
                        </li>
                        <li class="nav-item" role="presentation" style="width: 50%;">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Product</a>
                        </li>

                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                        <div class="row">
                            {{-- awal col --}}
                <div class="col-sm-6 col-lg-4">
                    <div class="card-box">
                        <div class="contact-card">
                            <a class="pull-left" href="#">
                            <img class="rounded" src="{{ asset('/uploads/fotouser/user.jpg')}}" alt="user">
                            </a>
                            <div class="member-info">
                                <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                <p class="text-muted">Branch manager</p>
                                <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                
                            </div>

                            <ul class="social-links list-inline m-0">
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                </li>
                            </ul>

                        </div>
                        <a href="{{url('/member/detail/1')}}" class="btn btn-primary mt-4" style="width: 100%;">Detail</a>
            </div>
          </div> 
       <!-- end col -->
                            {{-- awal col --}}
                <div class="col-sm-6 col-lg-4">
                    <div class="card-box">
                        <div class="contact-card">
                            <a class="pull-left" href="#">
                            <img class="rounded" src="{{ asset('/uploads/fotouser/user.jpg')}}" alt="user">
                            </a>
                            <div class="member-info">
                                <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                <p class="text-muted">Branch manager</p>
                                <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                
                            </div>

                            <ul class="social-links list-inline m-0">
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                </li>
                            </ul>

                        </div>
                        <a href="{{url('/member/detail/1')}}" class="btn btn-primary mt-4" style="width: 100%;">Detail</a>
            </div>
          </div> 
       <!-- end col -->
                            {{-- awal col --}}
                <div class="col-sm-6 col-lg-4">
                    <div class="card-box">
                        <div class="contact-card">
                            <a class="pull-left" href="#">
                            <img class="rounded" src="{{ asset('/uploads/fotouser/user.jpg')}}" alt="user">
                            </a>
                            <div class="member-info">
                                <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                <p class="text-muted">Branch manager</p>
                                <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                
                            </div>

                            <ul class="social-links list-inline m-0">
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                </li>
                            </ul>

                        </div>
                        <a href="{{url('/member/detail/1')}}" class="btn btn-primary mt-4" style="width: 100%;">Detail</a>
            </div>
          </div> 
       <!-- end col -->
                            {{-- awal col --}}
                <div class="col-sm-6 col-lg-4">
                    <div class="card-box">
                        <div class="contact-card">
                            <a class="pull-left" href="#">
                            <img class="rounded" src="{{ asset('/uploads/fotouser/user.jpg')}}" alt="user">
                            </a>
                            <div class="member-info">
                                <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                <p class="text-muted">Branch manager</p>
                                <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                
                            </div>

                            <ul class="social-links list-inline m-0">
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                </li>
                            </ul>

                        </div>
                        <a href="{{url('/member/detail/1')}}" class="btn btn-primary mt-4" style="width: 100%;">Detail</a>
            </div>
          </div> 
       <!-- end col -->
                            {{-- awal col --}}
                <div class="col-sm-6 col-lg-4">
                    <div class="card-box">
                        <div class="contact-card">
                            <a class="pull-left" href="#">
                            <img class="rounded" src="{{ asset('/uploads/fotouser/user.jpg')}}" alt="user">
                            </a>
                            <div class="member-info">
                                <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                <p class="text-muted">Branch manager</p>
                                <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                
                            </div>

                            <ul class="social-links list-inline m-0">
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                </li>
                            </ul>

                        </div>
                        <a href="{{url('/member/detail/1')}}" class="btn btn-primary mt-4" style="width: 100%;">Detail</a>
            </div>
          </div> 
       <!-- end col -->
                            {{-- awal col --}}
                <div class="col-sm-6 col-lg-4">
                    <div class="card-box">
                        <div class="contact-card">
                            <a class="pull-left" href="#">
                            <img class="rounded" src="{{ asset('/uploads/fotouser/user.jpg')}}" alt="user">
                            </a>
                            <div class="member-info">
                                <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                <p class="text-muted">Branch manager</p>
                                <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                
                            </div>

                            <ul class="social-links list-inline m-0">
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                </li>
                            </ul>

                        </div>
                        <a href="{{url('/member/detail/1')}}" class="btn btn-primary mt-4" style="width: 100%;">Detail</a>
            </div>
          </div> 
       <!-- end col -->
                            {{-- awal col --}}
                <div class="col-sm-6 col-lg-4">
                    <div class="card-box">
                        <div class="contact-card">
                            <a class="pull-left" href="#">
                            <img class="rounded" src="{{ asset('/uploads/fotouser/user.jpg')}}" alt="user">
                            </a>
                            <div class="member-info">
                                <h4 class="m-t-0 m-b-5 header-title"><b>Bill Bertz</b></h4>
                                <p class="text-muted">Branch manager</p>
                                <p class="text-dark"><i class="md md-business m-r-10"></i><small>ABC company Pvt Ltd.</small></p>
                                
                            </div>

                            <ul class="social-links list-inline m-0">
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Message"><i class="fa fa-envelope-o"></i></a>
                                </li>
                            </ul>

                        </div>
                        <a href="{{url('/member/detail/1')}}" class="btn btn-primary mt-4" style="width: 100%;">Detail</a>
            </div>
          </div> 
       <!-- end col -->
                        </div>
                        
                        </p>
                    </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">halaman productnbksf</div>

               </div>
   </div>
 </div>
</div>
    
@endsection