@extends('welcome')

@section('title')
    LapakUsahaKita.com  - Activity Liburan
@endsection

@section('content')

<div class="container">
    <div class="row">
       <div class="col-md-12">
           <div class="card">
               <div class="card-body">
                   <div class="card-title">
                       <h1>Activity Liburan  <a data-toggle="modal" data-target="#infomodal"><i class="fa fa-info-circle"></i></a></h1>
                   </div>
                   <div class="clearfix">
                       <div class="float-right">
                           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addactivity">
                            Add Activity Liburan
                            </button>
                       </div>
                   </div>
                    <div class="table-responsive mt-1">    
                        <table id="data-table" class="table" width="100%"ß>
                                <thead>
                                   <th>Berapa Hari</th>
                                   <th>Type Of Trip</th>
                                    <th>To</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                        </table>
                    </div>
               </div>
           </div>
       </div>
    </div>
</div>


{{-- awal modal --}}
<div class="modal fade" id="addactivity" tabindex="-1" aria-labelledby="addactivityLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addactivityLabel">Add Activity Liburan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formInsert" method="post">

   
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                  <input type="hidden" name="iduser" id="iduser">
                    <label for="">To : <span style="color:red;">*</span>  </label>
                    <select name="to" class="form-control" >
    
                        <option value="JawaBarat">Jawa Barat</option>
                        <option value="JawaTimur">Jawa Timur</option>
                        <option value="sumatra">Sumatra</option>
                        <option value="jakarta">jakarta</option>
                        <option value="bogor">bogor</option>
                        <option value="sulawasi">sulawasi</option>
                        <option value="papua">papua</option>
                        <option value="sumatra">Sumatra</option>
                        <option value="kalimantan">kalimantan</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">berapa hari : <span style="color:red;">*</span> </label>
                    <input type="text" class="form-control"   name="berapa_hari" id="berapa_hari">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Type Of Trip: <span style="color:red;">*</span> </label>
                    <input type="text" class="form-control"   name="type_of_trip" id="type_of_trip">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Select Rencana Kengiatanmu: <span style="color:red;">*</span> </label>
                    <select name="id_name_rencana_kegiatan[]" multiple id="id_name_rencana_kegiatan" class="form-control" required>
                      <option value="">Not Selected</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Select Alat - Alat Kengiatan mu: <span style="color:red;">*</span> </label>
                            <select name="id_alatkegiatan[]" multiple id="id_alatkegiatan" class="form-control" >
                                <option value="">Not Selected</option>
                             </select>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
         </form>
    </div>
  </div>
</div>

{{-- akhir add --}}

{{-- awal modal --}}
<div class="modal fade" id="updateactivity" tabindex="-1" aria-labelledby="updateactivityLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateactivityLabel">Update Activity Liburan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formUpdate" method="post">

   
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                   <input type="hidden" name="iduse" id="iduser_edit">
                    <label for="">To : <span style="color:red;">*</span> </label>
                    <select name="to" id="to" class="form-control" required>
                        <option value="">Jawa Barat</option>
                        <option value="">Jawa Timur</option>
                        <option value="">Sumatra</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">berapa hari <span style="color:red;">*</span> </label>
                    <input type="text" class="form-control" required name="to" id="to">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Type Of Trip <span style="color:red;">*</span> </label>
                    <input type="text" class="form-control" required name="to" id="to">
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
         </form>
    </div>
  </div>
</div>

{{-- akhir add --}}

{{-- info modal --}}


<div class="modal fade" id="infomodal" tabindex="-1" aria-labelledby="infomodalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="infomodalLabel">Info Activity Liburan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Selamat Datang Halaman Activity Liburan, di halaman ini anda akan meng menambahkan rencana liburan anda </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

{{-- akhir modal --}}


<script>
    $(document).ready(function () {
        DataTable();
        AddActivityLiburan();
        UpdateActivityLiburan();
        getListRencana();
       getListAlatkengiatan();
       $('#iduser').val(localStorage.getItem('id'));
    });


    function DataTable() {
      let id =  localStorage.getItem('id');
      $('#data-table').DataTable({
          processing: true,
          serverSide: true,
          destroy: true,
          orderable: false, 
          searchable: false,
          ajax: `{{ url('/rencana/activityliburan/${id}') }}`,
          columns: [
              {data: 'berapa_hari'},
              {data: 'type_of_trip'},
              {data: 'to'},
              {data: 'created_at'},
              {data: 'updated_at'},
              {data: 'action'},
          ]
      });
	}

    function AddActivityLiburan() {
      
        $('#formInsert').submit(function (e) { 
            e.preventDefault();
            $.ajax({
              type: "post",
              url: `{{url('/api/activityliburan/store_new')}}`,
              data: $('#formInsert').serialize(),
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Menambah Data!',
                    'success'
                  )
                   $('#addactivity').modal('hide');
                   DataTable(); 
                }
              }
            });           
        });
    }

    
     function getdataedit(id) {
        
          $.ajax({
            type: "get",
            url: `{{url('/api/jenisusaha/edit/${id}')}}`,
            dataType: "JSON",
            success: function (response) {
              let {name_jenis_usaha,id} = response.data;
              if (response.status) {
                $('#id').val(id);
                $('#name_jenis_usaha_edit').val(name_jenis_usaha);
              }
            }
          });

    }

    function UpdateActivityLiburan() {
        $('#formUpdate').submit(function (e) { 
            e.preventDefault();
            $.ajax({
              type: "put",
              url: `{{url('/api/jenisusaha/update')}}`,
              data: $('#formUpdate').serialize(),
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Update Data!',
                    'success'
                  )
                  $('#updateactivity').modal('hide');
                  DataTable();
                  $('#nama_usaha_edit').val('');                
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Update Data!',
                    'error'
                  )
                }
              }
            });   
        });
    }

     function deletedata(id) {
            $.ajax({
              type: "delete",
              url: `{{url('/api/activityliburan/delete/${id}')}}`,
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Deletes Data!',
                    'success'
                  )
                  DataTable();
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Deletes Data!',
                    'error'
                  )
                }
              }
            });    
    }

    function getListRencana() {
     $.ajax({
       type: "get",
       url: `{{url('/api/rencana/allDataRencana')}}`,
       dataType: "JSON",
       success: function (response) {
         if (response) {
           response.data.forEach(function(item) {
              $('#id_name_rencana_kegiatan').append(
                `
                 <option style="color: black !important;" value="${item.id}">${item.name_kegiatan}</option>
                `
                )
           })
         }
       }
     });

     setTimeout(() => {
      $('#id_name_rencana_kegiatan').selectpicker();
     }, 2000);
    }



    function getListAlatkengiatan() {

      
       $.ajax({
       type: "get",
       url: `{{url('/api/alat/allDataAlat')}}`,
       dataType: "JSON",
       success: function (response) {
         if (response) {
           response.data.forEach(function(item) {
              $('#id_alatkegiatan').append(
                `
                 <option style="color: black !important;"  value="${item.id}">${item.name_alat}</option>
                `
                )
           })
         }
       }
     });
     setTimeout(() => {
       
      $('#id_alatkegiatan').selectpicker();
     }, 2000);
    }
</script>
{{-- akhir modal --}}
@endsection