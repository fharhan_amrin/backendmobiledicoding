@extends('welcome')

@section('title')
   LapakUsahaKita.com - Alat Kegiatan
@endsection

@section('content')

<div class="container">
    <div class="row">
       <div class="col-md-12">
           <div class="card">
               <div class="card-body">
                   <div class="card-title">
                       <h1> Alat Kengiatan <a data-toggle="modal" data-target="#infomodal"><i class="fa fa-info-circle"></i></a></h1>
                   </div>
                   <div class="clearfix">
                       <div class="float-right">
                           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addactivity">
                            Add Alat  Kegiatan
                            </button>
                       </div>
                   </div>
                    <div class="table-responsive mt-1">    
                        <table id="data-table" class="table" width="100%"ß>
                                <thead>
                                   
                                    <th>Name Alat Kegiatan</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                        </table>
                    </div>
               </div>
           </div>
       </div>
    </div>
</div>


{{-- awal modal --}}
<div class="modal fade" id="addactivity" tabindex="-1" aria-labelledby="addactivityLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addactivityLabel">Add Alat Kengiatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formInsert" method="post">

   
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Name : <span style="color:red;">*</span>  </label>
                    <input type="text" id="name_alat" name="name_alat" class="form-control">
                </div>
            </div>
           
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
         </form>
    </div>
  </div>
</div>

{{-- akhir add --}}

{{-- awal modal --}}
<div class="modal fade" id="updateactivity" tabindex="-1" aria-labelledby="updateactivityLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateactivityLabel">Update Alat Kengiatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formUpdate" method="post">

   
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Name : <span style="color:red;">*</span> </label>
                    <input type="hidden" id="id" class="form-control" name="id">
                    <input type="text" id="name_alat_edit" class="form-control" name="name_alat">
                </div>
            </div>
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
         </form>
    </div>
  </div>
</div>

{{-- akhir add --}}


{{-- info modal --}}


<div class="modal fade" id="infomodal" tabindex="-1" aria-labelledby="infomodalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="infomodalLabel">Info Alat Kengiatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Selamat Datang Halaman Alat Kengiatan, di halaman ini anda akan mempersiapkan alat-alat yang pengingat yang harus di bawa dan biasanya di bawa, untuk di add di menu aktivity liburan </p>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

{{-- akhir modal --}}


<script>
    $(document).ready(function () {
        DataTable();
        AddActivityLiburan();
        UpdateActivityLiburan();
    });


    function DataTable() {
      $('#data-table').DataTable({
          processing: true,
          serverSide: true,
          destroy: true,
          orderable: false, 
          searchable: false,
          ajax: "{{ url('/rencana/alatkegiatan') }}",
          columns: [
              {data: 'name_alat'},
              {data: 'created_at'},
              {data: 'updated_at'},
              {data: 'action'},
          ]
      });
	}

    function AddActivityLiburan() {
        $('#formInsert').submit(function (e) { 
            e.preventDefault();
            $.ajax({
              type: "post",
              url: `{{url('/api/alat/store')}}`,
              data: $('#formInsert').serialize(),
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Menambah Data!',
                    'success'
                  )
                   $('#addactivity').modal('hide');
                   DataTable(); 
                }
              }
            });           
        });
    }

    
     function getdataedit(id) {
        
          $.ajax({
            type: "get",
            url: `{{url('/api/alat/edit/${id}')}}`,
            dataType: "JSON",
            success: function (response) {
              let {id,name_alat} = response.data;
              if (response.status) {
                $('#id').val(id);
                $('#name_alat_edit').val(name_alat);
              }
            }
          });

    }

    function UpdateActivityLiburan() {
        $('#formUpdate').submit(function (e) { 
            e.preventDefault();
            $.ajax({
              type: "put",
              url: `{{url('/api/alat/update')}}`,
              data: $('#formUpdate').serialize(),
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Update Data!',
                    'success'
                  )
                  $('#updateactivity').modal('hide');
                  DataTable();               
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Update Data!',
                    'error'
                  )
                }
              }
            });   
        });
    }

     function deletedata(id) {
            $.ajax({
              type: "delete",
              url: `{{url('/api/alat/delete/${id}')}}`,
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Deletes Data!',
                    'success'
                  )
                  DataTable();
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Deletes Data!',
                    'error'
                  )
                }
              }
            });    
    }
</script>

{{-- akhir modal --}}
@endsection