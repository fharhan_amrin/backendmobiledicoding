@extends('welcome')

@section('title')
    LapakUsahaKita.com - Notifikasi Kengiatan
@endsection

@section('content')

<div class="container">
    <div class="row">
       <div class="col-md-12">
           <div class="card">
               <div class="card-body">
                   <div class="card-title">
                       <h1> Notifikasi Kengiatan <a data-toggle="modal" data-target="#infomodal"><i class="fa fa-info-circle"></i></a></</h1>
                   </div>
                   <div class="clearfix">
                       <div class="float-right">
                           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addactivity">
                            Add Notifikasi alarm
                            </button>
                       </div>
                   </div>
                    <div class="table-responsive mt-1">    
                        <table id="data-table" class="table" width="100%"ß>
                                <thead>
                                    <th>Activity Liburan</th>
                                    <th>Mulai Waktu</th>
                                    <th>Selesai Waktu</th>
                                    <th>Status Notifikasi</th>
                                    <th>Per h-1 , per jam</th>
                                    <th>Notivikasi Via</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                        </table>
                    </div>
               </div>
           </div>
       </div>
    </div>
</div>


{{-- awal modal --}}
<div class="modal fade" id="addactivity" tabindex="-1" aria-labelledby="addactivityLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addactivityLabel">Add Notifikasi Alarm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formInsert" method="post">

   
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Mulai Dari : <span style="color:red;">*</span>  </label>
                    <input type="hidden" id="id_user" name="id_user" class="form-control" required>
                    <input type="date" id="start_date" name="start_date" class="form-control" required>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Selesai Waktu : <span style="color:red;">*</span>  </label>
                    <input type="date" id="end_date" name="end_date" class="form-control"  required>
                </div>
            </div>
        </div>

        <div class="row">
             <div class="col-md-6">
                <div class="form-group">
                    <label for="">Select Notifikasi : <span style="color:red;">*</span>  </label>
                    <select name="notifikasi_via" id="notifikasi_via" class="form-control" required>
                        <option value="">Not Selected</option>
                        <option value="Email">Email</option>
                        <option value="Telegram">Telegram</option>
                    </select>
                </div>
            </div>

             <div class="col-md-6">
                <div class="form-group">
                    <label for="">Kirim Per : <span style="color:red;">*</span>  </label>
                    <select name="per" id="per" class="form-control" required>
                        <option value="">Not Selected</option>
                        <option value="Per Hari">Per Hari</option>
                        <option value="Per 5 Jam">Per 5 Jam</option>
                        <option value="Per 1 Jam">Per 1 Jam</option>
                    </select>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Select Activity : <span style="color:red;">*</span>  </label>
                    <select name="id_activity_kengiatan" id="id_activity_kengiatan" class="form-control" required>
                        <option value="">Not Selected</option>
                        <option value="1">Mancing Belut</option>
                        <option value="2">Akhir Pekan</option>
                        <option value="3">Apalah</option>
                    </select>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
         </form>
    </div>
  </div>
</div>

{{-- akhir add --}}

{{-- awal modal --}}
<div class="modal fade" id="updateactivity" tabindex="-1" aria-labelledby="updateactivityLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateactivityLabel">Update Notifikasi Alarm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formUpdate" method="post">

   
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Mulai Dari : <span style="color:red;">*</span>  </label>
                    <input type="hidden" id="id" name="id" class="form-control">
                    <input type="date" id="start_date_edit" name="start_date" class="form-control">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Selesai Waktu : <span style="color:red;">*</span>  </label>
                    <input type="date" id="end_date_edit" name="end_date" class="form-control">
                </div>
            </div>
        </div>

        <div class="row">
             <div class="col-md-6">
                <div class="form-group">
                    <label for="">Select Notifikasi : <span style="color:red;">*</span>  </label>
                    <select name="notifikasi_via" id="notifikasi_via_edit" class="form-control">
                        <option value="">Not Selected</option>
                        <option value="email">Email</option>
                        <option value="telegram">Telegram</option>
                    </select>
                </div>
            </div>

             <div class="col-md-6">
                <div class="form-group">
                    <label for="">Kirim Per : <span style="color:red;">*</span>  </label>
                    <select name="notifikasi_via" id="notifikasi_via" class="form-control">
                        <option value="">Not Selected</option>
                        <option value="Per Hari">Per Hari</option>
                        <option value="Per 5 Jam">Per 5 Jam</option>
                        <option value="Per 1 Jam">Per 1 Jam</option>
                    </select>
                </div>
            </div>
             <div class="col-md-6">
                <div class="form-group">
                    <label for=""> Per : <span style="color:red;">*</span>  </label>
                    <select name="id_activity_kengiatan" id="id_activity_kengiatan_edit" class="form-control">
                        <option value="">Not Selected</option>
                        <option value="1">Per Hari</option>
                        <option value="2">Per 5 Jam</option>
                        <option value="3">Per 1 Jam</option>
                    </select>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
         </form>
    </div>
  </div>
</div>

{{-- akhir add --}}



{{-- info modal --}}


<div class="modal fade" id="infomodal" tabindex="-1" aria-labelledby="infomodalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="infomodalLabel">Info Notifikasi Kengiatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <p>Selamat Datang Halaman Notifikasi Liburan, di halaman ini anda akan menambahkan aktivitas yang anda sudah tambahkan di menu aktivity liburan, untuk membuat notifikasi email atau secara bot telegram sebagai pengingat anda, </p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

{{-- akhir modal --}}


<script>
    $(document).ready(function () {
        $('#id_user').val(localStorage.getItem('id'));
        DataTable();
        AddActivityLiburan();
        UpdateActivityLiburan();
    });


    function DataTable() {
      let id    = localStorage.getItem('id');
      $('#data-table').DataTable({
          processing: true,
          serverSide: true,
          destroy: true,
          orderable: false, 
          searchable: false,
          ajax: `{{ url('/rencana/notifikasikegiatan/${id}') }}`,
          columns: [
              {data: 'id_activity_kengiatan'},
              {data: 'start_date'},
              {data: 'end_date'},
              {data: 'status_pesan'},
              {data: 'per'},
              {data: 'notifikasi_via'},
              {data: 'action'},
          ]
      });
	}

    function AddActivityLiburan() {
        $('#formInsert').submit(function (e) { 
            e.preventDefault();
            $.ajax({
              type: "post",
              url: `{{url('/api/notifikasi/store')}}`,
              data: $('#formInsert').serialize(),
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Menambah Data!',
                    'success'
                  )
                   $('#addactivity').modal('hide');
                   DataTable(); 
                }
              }
            });           
        });
    }

    
     function getdataedit(id) {
        
          $.ajax({
            type: "get",
            url: `{{url('/api/notifikasi/edit/${id}')}}`,
            dataType: "JSON",
            success: function (response) {
              let {id,id_activity_kengiatan,start_date,end_date,per,status_pesan,notifikasi_via} = response.data;
              if (response.status) {
                $('#id').val(id);
                $('#name_jenis_usaha_edit').val(name_jenis_usaha);
              }
            }
          });

    }

    function UpdateActivityLiburan() {
        $('#formUpdate').submit(function (e) { 
            e.preventDefault();
            $.ajax({
              type: "put",
              url: `{{url('/api/notifikasi/update')}}`,
              data: $('#formUpdate').serialize(),
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Update Data!',
                    'success'
                  )
                  $('#updateactivity').modal('hide');
                  DataTable();               
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Update Data!',
                    'error'
                  )
                }
              }
            });   
        });
    }

     function deletedata(id) {
            $.ajax({
              type: "delete",
              url: `{{url('/api/notifikasi/delete/${id}')}}`,
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Deletes Data!',
                    'success'
                  )
                  DataTable();
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Deletes Data!',
                    'error'
                  )
                }
              }
            });    
    }
</script>

{{-- akhir modal --}}
@endsection