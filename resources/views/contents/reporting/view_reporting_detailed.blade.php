@extends('welcome')

@section('title')
    LapakUsahaKita.com - Reporting
@endsection

@section('content')

<div class="container">
    <div class="row">
       <div class="col-md-12">
           <div class="card">
               <div class="card-body">
                   <div class="card-title">
                       <h1>  Reporting <a data-toggle="modal" data-target="#infomodal"><i class="fa fa-info-circle"></i></a></h1>
                   </div>
                   <div class="row">
                       <div class="col-md-12">
                           <div class="card">
                               <div class="card-body">
                                  <h6>Tahap Pengembangan selanjutnya......</h6>
                               </div>
                           </div>
                       </div>
                      
                   </div>

                   <div class="row mt-2">
                       <div class="col-md-12">
                           <div class="card">
                               <div class="card-body">
                                   <div id="transaksiHarian"></div>
                               </div>
                           </div>
                       </div>
                   </div>
                  
                   
               </div>
           </div>
       </div>
    </div>
</div>




{{-- info modal --}}


<div class="modal fade" id="infomodal" tabindex="-1" aria-labelledby="infomodalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="infomodalLabel">Info Reporting</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo, sequi eius. Reprehenderit soluta alias aspernatur atque eligendi perspiciatis repellat laboriosam doloremque in mollitia suscipit saepe, repellendus molestias cupiditate id quidem!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

{{-- akhir modal --}}





{{-- akhir modal --}}
@endsection