@extends('welcome')

@section('title')
    LapakUsahaKita.com - Sosial Media
@endsection

@section('content')

<div class="container">
    <div class="row">
       <div class="col-md-12">
           <div class="card">
               <div class="card-body">
                   <div class="card-title">
                       <h1> Sosial Media <a data-toggle="modal" data-target="#infomodal"><i class="fa fa-info-circle"></i></a></h1>
                   </div>
                   {{-- <div class="clearfix">
                       <div class="float-right">
                           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addactivity">
                            Tambah Sosial Media
                            </button>
                       </div>
                   </div> --}}
                    <div class="table-responsive mt-1">    
                        <table  class="table data-table" width="100%"ß>
                                <thead>
                                    <th>No</th>
                                    <th>Name Sosial Media</th>
                                    <th>Action</th>
                                </thead>
                                <tbody id="data">
                                    
                                </tbody>
                        </table>
                    </div>
               </div>
           </div>
       </div>
    </div>
</div>


{{-- awal modal --}}
<div class="modal fade" id="addactivity" tabindex="-1" aria-labelledby="addactivityLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addactivityLabel">Tambah Sosial Media</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formInsert" method="post">

   
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Name : <span style="color:red;">*</span>  </label>
                    <input type="text" id="name" name="name" class="form-control">
                </div>
            </div>
           
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
         </form>
    </div>
  </div>
</div>

{{-- akhir add --}}

{{-- awal modal --}}
<div class="modal fade" id="updateactivity" tabindex="-1" aria-labelledby="updateactivityLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateactivityLabel">Update Sosial Media</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formUpdate" method="post">

   
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Name : <span style="color:red;">*</span> </label>
                    <input type="hidden" id="id"  class="form-control" name="id">
                    <input type="text" id="name_sosial_media_edit"  class="form-control" name="name_sosial_media">
                </div>
            </div>
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
         </form>
    </div>
  </div>
</div>

{{-- akhir add --}}


{{-- info modal --}}


<div class="modal fade" id="infomodal" tabindex="-1" aria-labelledby="infomodalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="infomodalLabel">Info Sosial Media</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo, sequi eius. Reprehenderit soluta alias aspernatur atque eligendi perspiciatis repellat laboriosam doloremque in mollitia suscipit saepe, repellendus molestias cupiditate id quidem!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

{{-- akhir modal --}}



<script>
    $(document).ready(function () {
        DataTable();
        AddActivityLiburan();
        UpdateActivityLiburan();
        
    });

   function DataTable() {
		$('.data-table').DataTable({
        processing: true,
        serverSide: true,
         destroy: true,
         ajax: "{{ url('/sosialmedia') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name_sosial_media', name: 'name_sosial_media'},
            {data: 'action', name: 'action',
			 orderable: false, 
			 searchable: false
			 },
        ]
    });
	}

    function AddActivityLiburan() {
        $('#formInsert').submit(function (e) { 
            e.preventDefault();

            alert("tutup aplikasi saat add");

            $('#addactivity').modal('hide');
            
        });
    }


    function getdataedit(id) {
      $.ajax({
        type: "get",
        url: `{{url('/api/sosialmedia/edit/${id}')}}`,
        dataType: "JSON",
        success: function (response) {
          let {name_sosial_media,id} = response.data;
          if (response.status) {
            $('#id').val(id);
            $('#name_sosial_media_edit').val(name_sosial_media);
          }
        }
      });
    }

    function UpdateActivityLiburan() {
        $('#formUpdate').submit(function (e) { 
            e.preventDefault();

            $.ajax({
              type: "put",
              url: `{{url('/api/sosialmedia/update')}}`,
              data: $('#formUpdate').serialize(),
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Update Data!',
                    'success'
                  )
                  $('#updateactivity').modal('hide');
                  DataTable();
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Update Data!',
                    'error'
                  )
                }
              }
            });

            
            
        });
    }

     function deletedata(id) {
      

            $.ajax({
              type: "delete",
              url: `{{url('/api/sosialmedia/delete/${id}')}}`,
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Deletes Data!',
                    'success'
                  )
                
                  DataTable();
                  setTimeout(() => { 
                  $('#sosialmedia').DataTable();
                }, 2000);
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Deletes Data!',
                    'error'
                  )
                }
              }
            });

            
            
       
    }
</script>

{{-- akhir modal --}}
@endsection