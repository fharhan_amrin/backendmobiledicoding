@extends('welcome')

@section('title')
    LapakUsahaKita.com
@endsection

@section('content')
<div class="row">
                            <div class="col-md-4 col-lg-3">
                                <div class="profile-detail card-box">
                                    <div>
                                        <img src="{{url('')}}/dashboard/assets/images/users/avatar-2.jpg" class="rounded-circle" alt="profile-image">

                                        <ul class="list-inline status-list m-t-20">
                                            <li class="list-inline-item">
                                                <h3 class="text-primary m-b-5">456</h3>
                                                <p class="text-muted">Followings</p>
                                            </li>

                                            <li class="list-inline-item">
                                                <h3 class="text-success m-b-5">5864</h3>
                                                <p class="text-muted">Followers</p>
                                            </li>
                                        </ul>

                                        <button type="button" class="btn btn-pink btn-custom btn-rounded waves-effect waves-light">Follow</button>

                                        <hr>
                                        <h4 class="text-uppercase font-18 font-600">About Me</h4>
                                        <p class="text-muted font-13 m-b-30">
                                            Hi I'm Johnathn Deo,has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                                        </p>

                                        <div class="text-left">
                                            <p class="text-muted font-13"><strong>Full Name :</strong> <span class="m-l-15">Johnathan Deo</span></p>

                                            <p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15">(123) 123 1234</span></p>

                                            <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">coderthemes@gmail.com</span></p>

                                            <p class="text-muted font-13"><strong>Location :</strong> <span class="m-l-15">USA</span></p>

                                        </div>


                                        <div class="button-list m-t-20">
                                            <button type="button" class="btn btn-facebook waves-effect waves-light">
                                                <i class="fa fa-facebook"></i>
                                            </button>

                                            <button type="button" class="btn btn-twitter waves-effect waves-light">
                                                <i class="fa fa-twitter"></i>
                                            </button>

                                            <button type="button" class="btn btn-linkedin waves-effect waves-light">
                                                <i class="fa fa-linkedin"></i>
                                            </button>

                                            <button type="button" class="btn btn-dribbble waves-effect waves-light">
                                                <i class="fa fa-dribbble"></i>
                                            </button>

                                        </div>
                                    </div>

                                </div>

                                <div class="card-box">
                                    <h4 class="m-t-0 m-b-20 header-title"><b>Friends <span class="text-muted">(154)</span></b></h4>

                                    <div class="friend-list">
                                        <a href="#">
                                            <img src="{{url('')}}/dashboard/assets/images/users/avatar-1.jpg" class="rounded-circle thumb-md" alt="friend">
                                        </a>

                                        <a href="#">
                                            <img src="{{url('')}}/dashboard/assets/images/users/avatar-2.jpg" class="rounded-circle thumb-md" alt="friend">
                                        </a>

                                        <a href="#">
                                            <img src="{{url('')}}/dashboard/assets/images/users/avatar-3.jpg" class="rounded-circle thumb-md" alt="friend">
                                        </a>

                                        <a href="#">
                                            <img src="{{url('')}}/dashboard/assets/images/users/avatar-4.jpg" class="rounded-circle thumb-md" alt="friend">
                                        </a>

                                        <a href="#">
                                            <img src="{{url('')}}/dashboard/assets/images/users/avatar-5.jpg" class="rounded-circle thumb-md" alt="friend">
                                        </a>

                                        <a href="#">
                                            <img src="{{url('')}}/dashboard/assets/images/users/avatar-6.jpg" class="rounded-circle thumb-md" alt="friend">
                                        </a>

                                        <a href="#">
                                            <img src="{{url('')}}/dashboard/assets/images/users/avatar-7.jpg" class="rounded-circle thumb-md" alt="friend">
                                        </a>

                                        <button class="btn btn-primary">Show All</button>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-9 col-md-8">
                                <div class="card">
                                    <div class="card-body">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">halaman home
       <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque hic fugiat dolores recusandae voluptates ipsum ex? Voluptatem culpa temporibus repudiandae veritatis fuga quaerat esse minus molestiae, earum, iure necessitatibus cum.</p>
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">halaman profiler</div>
  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">halaman contact</div>
</div>
                                    </div>
                                </div>
                                
                            </div>

                        </div>
    
@endsection