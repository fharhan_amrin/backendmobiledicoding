@extends('welcome')

@section('title')
   LapakUsahaKita.com - Content
@endsection

@section('content')


<div class="container">
      <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                {{-- <div class="btn-group pull-right m-t-15">
                                    <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings</button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                                        <a class="dropdown-item" href="#">Dropdown One</a>
                                        <a class="dropdown-item" href="#">Dropdown Two</a>
                                        <a class="dropdown-item" href="#">Dropdown Three</a>
                                        <a class="dropdown-item" href="#">Dropdown Four</a>
                                    </div>
                                </div> --}}

                                <h4 class="page-title">Product Detail</h4>
                                <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/bukausahaproduct')}}">Luk</a></li>
                                     <li class="breadcrumb-item active">Product Detail</li>
                                </ol>

                            </div>
                        </div>

                        {{-- content detail --}}
                            <div class="row">
                            <div class="col-12">
                                <div class="card-box product-detail-box">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            {{-- <div class="sp-loading"><img src="assets/images/sp-loading.gif" alt=""><br>LOADING
                                                IMAGES
                                            </div> --}}
                                            <div class="sp-wrap">
                                                <a src="#">
                                                    <img src="{{asset('')}}uploads/product/{{$productdetail->image_product}}" alt="" style="width: 70%;"></a>
                                            </div>
                                        </div>

                                        <div class="col-sm-8">
                                            <div class="product-right-info">
                                            <h4><b>{{$productdetail->name_product}}</b></h4>
                                                <div class="rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><a class="fa fa-star" href=""></a></li>
                                                        <li class="list-inline-item"><a class="fa fa-star" href=""></a></li>
                                                        <li class="list-inline-item"><a class="fa fa-star" href=""></a></li>
                                                        <li class="list-inline-item"><a class="fa fa-star" href=""></a></li>
                                                        <li class="list-inline-item"><a class="fa fa-star-o" href=""></a></li>
                                                    </ul>
                                                </div>

                                                <h2> Rp. <b>{{$productdetail->stock_product}}</b></h2>

                                            <h5 class="m-t-20"><b>Stock: </b> {{$productdetail->stock_product}} pcs. <span class="label label-default m-l-5">
                                                <?php

                                                 if ($productdetail->status_product==0) {
                                                     echo "Tersedia";
                                                 }elseif ($productdetail->status_product==1) {
                                                    echo "Habis";
                                                }else{
                                                    echo "Segera Hadir";

                                                 }
                                                    
                                                ?>    
</span></h5>

                                                <hr/>

                                                <h5 class="font-600">Product Description</h5>

                                                <p class="text-muted">{{$productdetail->deskripsi_penjelasan_product}}.</p>

                                               

                                                <div class="m-t-30">
                                                   
                                                    <button type="button" onclick="addInvoice({{$productdetail->id}},{{$productdetail->harga_product}})" class="btn btn-danger waves-effect waves-light m-l-10">
                                                     <span class="btn-label"><i class="fa fa-shopping-cart"></i>
                                                   </span>Buy Now</button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->

                                    <div class="row m-t-30">
                                        <div class="col-12">
                                            <h4 class="font-18"><b>Specifications:</b></h4>
                                            <div class="table-responsive m-t-20">
                                                <table class="table">
                                                    <tbody>
                                                    <tr>
                                                        <td width="400">Created At</td>
                                                        <td>
                                                            {{$productdetail->created_at}}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Updated At</td>
                                                        <td>
                                                            {{$productdetail->updated_at}}
                                                        </td>
                                                    </tr>
                                                   
                                                 
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                                </div> <!-- end card-box/Product detai box -->
                            </div> <!-- end col -->
                        </div> <!-- end row -->
</div>

<script>
    // $(document).ready(function () {
        
    // });

    function addInvoice(id,harga) {
        let id_product  = id;
        let harga_product  = harga;
        let id_user  = localStorage.getItem('id');
        // console.log("id product", id);
        // console.log("harga product", harga_product);
         Swal.fire({
        title: 'Apakah kamu yakin?',
        text: "Data Ini akan Di masukan langsung ke invoice!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, to invoice'
        }).then((result) => {
        if (result.isConfirmed) {
    
            $.ajax({
                type: "POST",
                url: `{{url('api/addInvoice ')}}`,
                data: {
                    id_user:id_user,
                    id_product:id_product,
                    harga_product:harga_product
                },
                dataType: "JSON",
                success: function (response) {
                    if (response.status) {
                        Swal.fire(
                        'Berhasil !',
                        'Silahkan check langsung invoice anda',
                        'success'
                        )
                    }
                }
            });
        }
        })
    }
</script>


@endsection