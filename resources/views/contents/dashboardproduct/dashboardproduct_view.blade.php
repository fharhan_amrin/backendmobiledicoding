@extends('welcome')

@section('title')
   LapakUsahaKita.com - Content
@endsection

@section('content')

<style>
    svg{
        width: 10px !important;
    }
</style>

<div class="container">
    <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                               

                                <h4 class="page-title">Products</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">LUK</a></li>
                                    <li class="breadcrumb-item active">Products</li>
                                </ol>

                            </div>
                        </div>


                        <!-- SECTION FILTER
                        ================================================== -->
                        {{-- <div class="row m-t-10 m-b-10">

                            <div class="col-lg-6">
                                <form role="form">
                                    <div class="form-group contact-search m-b-0">
                                        <input type="text" id="search" class="form-control product-search" placeholder="Search here...">
                                        <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                                    </div> <!-- form-group -->
                                </form>
                            </div>

                            <div class="col-lg-6">
                                <div class="h5 m-0 text-right">
                                    <label class="vertical-middle m-r-10">Sort By:</label>
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-secondary active">
                                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Status
                                        </label>
                                        <label class="btn btn-secondary">
                                            <input type="radio" name="options" id="option2" autocomplete="off"> Type
                                        </label>
                                        <label class="btn btn-secondary">
                                            <input type="radio" name="options" id="option3" autocomplete="off"> Name
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div> --}}

                        {{-- end section filter --}}

                   {{-- content --}}
                  <div class="row m-b-15">

                    @foreach ($product as $item)
                        
                   

                            <div class="col-sm-6 col-lg-3 col-md-4 mobiles">
                                <div class="product-list-box thumb">
                                    <a href="{{url('/bukausahaproduct/detail/'.$item->id)}}" class="image-popup" title="Screenshot-1">
                                    <img src="{{asset('')}}uploads/product/{{$item->image_product}}" class="thumb-img" alt="work-thumbnail">
                                    </a>


                                    
                                    <div class="detail">
                                    <h4 class="font-18" style="color: black;"><a href="{{url('/bukausahaproduct/detail/'.$item->id)}}" >{{$item->name_product}}</a> </h4>
                                    <h4 class="font-18" style="color: #757575;">Stock : {{$item->stock_product}} pcs</span></h4>
                                    <h5 class="m-t-0 font-18" class="text-dark" style="color: #757575;">   Rp. {{$item->harga_product}}</h5>
                                    </div>
                                </div>
                            </div> 

                       @endforeach      
                   </div>        
                   
                   <div class="d-flex justify-content-center">
            {!! $product->links() !!}
        </div>

               {{-- end content --}}


</div>

@endsection