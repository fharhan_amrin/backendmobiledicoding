@extends('welcome')

@section('title')
   LapakUsahaKita.com - Content
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h1>Contents <a data-toggle="modal" data-target="#infomodal"><i class="fa fa-info-circle"></i></a></h1>
                        
                    </div>
                    <div class="clearfix">
                        <div class="float-right" style="margin-right: 10px;">
                            <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Tambah Contents</button>
                        </div>
                    </div>
                    <div class="table-responsive mt-1">
                        <table id="data-table" class="table" width="100%">
                            <thead>
                                <tr>
                                    <th>Judul Event</th>
                                    <th>Deskripsi Event</th>
                                    <th>Status Content</th>
                                    <th>Created Date</th>
                                    <th>Update Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              
                                    
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- add modal--}}

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Contents</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formInsert" method="post" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Judul : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="judul_event" name="judul_event" placeholder="Judul Event" >
                </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Deskripsi Event : <span style="color:red;">*</span></label>
                    <textarea type="text" class="form-control" id="deskripsi_event" name="deskripsi_event" placeholder="Enter deskripsi"   cols="20" rows="4"></textarea>
                   
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Foto Event : <span style="color:red;">*</span></label>
                    <input type="file" class="form-control" id="image_event" name="image_event" placeholder="Judul Event" >
                </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Penjelasan Link : <span style="color:red;">*</span></label>
                    <textarea name="link_vidio_penjelasan_event" id="link_vidio_penjelasan_event" cols="30" rows="5" class="form-control"> </textarea>
                   
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Link vidio free hiburan : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="link_vidio_free_hiburan" name="link_vidio_free_hiburan" placeholder="Enter Link" >
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Link event : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="link_event" name="link_event" placeholder="Enter Link" >
                </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Harga : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="harga" name="harga" placeholder="Rp."  >
                   
                </div>
            </div>

             <div class="col-md-6">
               <div class="form-group">
                    <label for="">Diskon : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="diskon" name="diskon" placeholder="Rp."  >
                   
                </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Status Content : <span style="color:red;">*</span></label>
                    <select name="status_content" id="status_content" class="form-control">
                        <option value="">All</option>
                        <option value="1">Release</option>
                        <option value="2">Pending</option>
                        <option value="3">Tutup</option>
                    </select>
                   
                </div>
            </div>

            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Type Jenis Content : <span style="color:red;">*</span></label>
                    <select name="type_jenis_content" id="type_jenis_content_new" class="form-control">
                        <option value="">All</option>
                       
                    </select>
                   
                </div>
            </div>

             <div class="col-md-6">
               <div class="form-group">
                    <label for="">Kapasitas Penonton : <span style="color:red;">*</span></label>
                    <input type="number" class="form-control" id="kapasitas_penonton" name="kapasitas_penonton">
                   
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
{{-- akhir add modal --}}

{{-- edit modal--}}

<div class="modal fade" id="exampleModaledit" tabindex="-1" aria-labelledby="exampleModaleditLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModaleditLabel">Update Contents</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formUpdate" method="post" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Judul : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="judul_event_edit" name="judul_event" placeholder="Judul Event" >
                    <input type="hidden" name="id" id="id_edit">
                </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Deskripsi Event : <span style="color:red;">*</span></label>
                    <textarea type="text" class="form-control" id="deskripsi_event_edit" name="deskripsi_event" placeholder="Enter deskripsi"   cols="20" rows="4"></textarea>
                   
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Foto Event : <span style="color:red;">*</span></label>
                     <input type="file" class="form-control" id="image_event_type_edit" name="image_event_type_edit" placeholder="Judul Event" >
                </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Penjelasan Link : <span style="color:red;">*</span></label>
                    <textarea name="link_vidio_penjelasan_event" id="link_vidio_penjelasan_event_edit" cols="30" rows="5" class="form-control"> </textarea>
                   
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Link vidio free hiburan : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="link_vidio_free_hiburan_edit" name="link_vidio_free_hiburan" placeholder="Enter Link" >
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Link event : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="link_event_edit" name="link_event" placeholder="Enter Link" >
                </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Harga : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="harga_edit" name="harga" placeholder="Rp."   >
                   
                </div>
            </div>

             <div class="col-md-6">
               <div class="form-group">
                    <label for="">Diskon : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="diskon_edit" name="diskon" placeholder="Rp."   >
                   
                </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Status Content : <span style="color:red;">*</span></label>
                    <select name="status_content" id="status_content_edit" class="form-control">
                        <option value="">All</option>
                        <option value="1">Release</option>
                        <option value="2">Pending</option>
                        <option value="3">Tutup</option>
                    </select>
                   
                </div>
            </div>

            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Type Jenis Content : <span style="color:red;">*</span></label>
                    <select name="type_jenis_content" id="type_jenis_content_edit" class="form-control">
                        
                    </select>
                   
                </div>
            </div>

             <div class="col-md-6">
               <div class="form-group">
                    <label for="">Kapasitas Penonton : <span style="color:red;">*</span></label>
                    <input type="number" class="form-control" id="kapasitas_penonton_edit" name="kapasitas_penonton">
                   
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
{{-- akhir edit modal --}}


{{-- detail modal--}}

<div class="modal fade" id="exampleModalDetail" tabindex="-1" aria-labelledby="exampleModaldetailLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModaldetailLabel">Detail Contents</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {{-- <form action="javascript:void(0)" id="formUpdate" method="post" enctype="multipart/form-data"> --}}
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Judul : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="judul_event_detail" readonly name="judul_event" placeholder="Judul Event" >
                    <input type="hidden" name="id" id="id">
                </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Deskripsi Event : <span style="color:red;">*</span></label>
                    <textarea type="text" class="form-control" id="deskripsi_event_detail" readonly name="deskripsi_event" placeholder="Enter deskripsi"   cols="20" rows="4"></textarea>
                   
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label style="display: block;" for="">Foto Event : <span style="color:red;">*</span></label>

                {{-- <img src="{{url('uploads/ScreenShot2020-12-01at17.08.47.png')}}" alt="image-event-detail" readonly style="width: 60%;"> --}}
                    <input type="text" class="form-control mt-1" id="image_event_detail" readonly name="image_event" placeholder="Judul Event" >
                </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Penjelasan Link : <span style="color:red;">*</span></label>
                    <textarea name="link_vidio_penjelasan_event" id="link_vidio_penjelasan_event_detail" readonly cols="30" rows="5" class="form-control"> </textarea>
                   
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Link vidio free hiburan : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="link_vidio_free_hiburan_detail" readonly name="link_vidio_free_hiburan" placeholder="Enter Link" >
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Link event : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="link_event_detail" readonly name="link_event" placeholder="Enter Link" >
                </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Harga : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="harga_detail" readonly name="harga" placeholder="Rp."   cols="20" rows="4">
                   
                </div>
            </div>

             <div class="col-md-6">
               <div class="form-group">
                    <label for="">Diskon : <span style="color:red;">*</span></label>
                    <input type="text" class="form-control" id="diskon_detail" readonly name="diskon" placeholder="Rp."   cols="20" rows="4">
                   
                </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Status Content : <span style="color:red;">*</span></label>
                    <select name="status_content" id="status_content_detail" readonly class="form-control">
                        <option value="">All</option>
                        <option value="1">Release</option>
                        <option value="2">Pending</option>
                        <option value="3">Tutup</option>
                    </select>
                   
                </div>
            </div>

            <div class="col-md-6">
               <div class="form-group">
                    <label for="">Type Jenis Content : <span style="color:red;">*</span></label>
                    <select name="type_jenis_content" id="type_jenis_content_detail" readonly class="form-control">
                        <option value="">All</option>
                        <option value="1">Hiburan</option>
                        <option value="2">Seni</option>
                        <option value="3">Karya</option>
                    </select>
                   
                </div>
            </div>

             <div class="col-md-6">
               <div class="form-group">
                    <label for="">Kapasitas Penonton : <span style="color:red;">*</span></label>
                    <input type="number" class="form-control" id="kapasitas_penonton_detail" readonly name="kapasitas_penonton">
                   
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      {{-- </form> --}}
    </div>
  </div>
</div>
{{-- akhir edit modal --}}

{{-- info modal --}}


<div class="modal fade" id="infomodal" tabindex="-1" aria-labelledby="infomodalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="infomodalLabel">Info Contents</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
        <p>Selamat Datang Di Halaman Content event , di sini anda akan  memasukan produk anda/usaha anda , di sini anda akan membuat event, penjelasan , tutorial, tips , trick  sesuai bidang jenis usaha anda ,secara gratis ataupun ber bayar untuk di promosikan di seluruh indonesia  berbentuk vidio, atau secara live streaming menggunakan google meet, live instagram, live youtube , vidio google drive dll  </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

{{-- akhir modal --}}



<script>
    $(document).ready(function () {
  
        DataTable();
        type_jenis_content_new();
        AddActivityLiburan();
        UpdateActivityLiburan();
    });


    function DataTable() {
      let id_user_pembuat = localStorage.getItem('id');
      $('#data-table').DataTable({
          processing: true,
          serverSide: true,
          destroy: true,
          orderable: false, 
          searchable: false,
          type:'get',
          ajax: `{{ url('/managecontent/${id_user_pembuat}') }}`,
          data:{
            id_user_pembuat:id_user_pembuat
          },
          columns: [
              {data: 'judul_event'},
              {data: 'deskripsi_event'},
              {data: 'status_content'},
              {data: 'created_at'},
              {data: 'updated_at'},
              {data: 'action'},
          ]
      });
	}

    function AddActivityLiburan() {
        $('#formInsert').submit(function (e) { 
            e.preventDefault();
            let id_user                      = localStorage.getItem('id');
            let judul_event                  = $('#judul_event').val();
            let deskripsi_event              = $('#deskripsi_event').val();
            let link_vidio_penjelasan_event  = $('#link_vidio_penjelasan_event').val();
            let link_vidio_free_hiburan      = $('#link_vidio_free_hiburan').val();
            let link_event                   = $('#link_event').val();
            let harga                        = $('#harga').val();
            let diskon                       = $('#diskon').val();
            let status_content               = $('#status_content').val();
            let type_jenis_content           = $('#type_jenis_content_new').val();
            let kapasitas_penonton           = $('#kapasitas_penonton').val();
            var file_data = $('#image_event').prop('files')[0];   
            var form_data = new FormData();                  
            form_data.append('image_event_type', file_data);
            form_data.append('id',id_user);
            form_data.append('judul_event',judul_event);
            form_data.append('deskripsi_event',deskripsi_event);
            form_data.append('link_vidio_penjelasan_event',link_vidio_penjelasan_event);
            form_data.append('link_vidio_free_hiburan',link_vidio_free_hiburan);
            form_data.append('link_event',link_event);
            form_data.append('harga',harga);
            form_data.append('diskon',diskon);
            form_data.append('status_content',status_content);
            form_data.append('type_jenis_content',type_jenis_content);
            form_data.append('kapasitas_penonton',kapasitas_penonton);
            $.ajax({
              type: "post",
              url: `{{url('/api/content/store')}}`,
              dataType: 'JSON',  // what to expect back from the PHP script, if anything
              cache: false,
              contentType: false,
              processData: false,
              data: form_data,
             
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Menambah Data!',
                    'success'
                  )
                   $('#exampleModal').modal('hide');
                   DataTable(); 
                }
              }
            });           
        });
    }

     function getdatadetail(id) {
        
          $.ajax({
            type: "get",
            url: `{{url('/api/content/edit/${id}')}}`,
            dataType: "JSON",
            success: function (response) {
              let {id,id_user_pembeli,id_user_pembuat,type_jenis_content,judul_event,deskripsi_event,image_event,link_vidio_penjelasan_event,link_vidio_free_hiburan,link_event,harga,diskon,status_content,kapasitas_penonton} = response.data;
              if (response.status) {
                $('#id').val(id);
                $('#type_jenis_content_detail').val(type_jenis_content);
                $('#judul_event_detail').val(judul_event);
                $('textarea#deskripsi_event_detail').val(deskripsi_event);
                $('#image_event_detail').val(image_event);
                $('textarea#link_vidio_penjelasan_event_detail').val(link_vidio_penjelasan_event);
                $('#link_vidio_free_hiburan_detail').val(link_vidio_free_hiburan);
                $('#link_event_detail').val(link_event);
                $('#harga_detail').val(harga);
                $('#diskon_detail').val(diskon);
                $('#status_content_detail').val(status_content);
                $('#kapasitas_penonton_detail').val(kapasitas_penonton);
              }
            }
          });

    }

    
     function getdataedit(id) {
        
          $.ajax({
            type: "get",
            url: `{{url('/api/content/edit/${id}')}}`,
            dataType: "JSON",
            success: function (response) {
             
              if (response.status) {
                $('#id_edit').val(id);
                $('#type_jenis_content_edit').val(response.data.type_jenis_content);
                $('#judul_event_edit').val(response.data.judul_event);
                $('textarea#deskripsi_event_edit').val(response.data.deskripsi_event);
                $('textarea#link_vidio_penjelasan_event_edit').val(response.data.ink_vidio_penjelasan_event);
                $('#link_vidio_free_hiburan_edit').val(response.data.link_vidio_free_hiburan);
                $('#link_event_edit').val(response.data.link_event);
                $('#harga_edit').val(response.data.harga);
                $('#diskon_edit').val(response.data.diskon);
                $('#status_content_edit').val(response.data.status_content);
                $('#kapasitas_penonton_edit').val(response.data.kapasitas_penonton);
              
               
              }
            }
          });

    }

    function UpdateActivityLiburan() {
        $('#formUpdate').submit(function (e) { 
            e.preventDefault(); 
            let id                           =  $('#id_edit').val();;
            let judul_event                  = $('#judul_event_edit').val();
            let deskripsi_event              = $('#deskripsi_event_edit').val();
            let link_vidio_penjelasan_event  = $('#link_vidio_penjelasan_event_edit').val();
            let link_vidio_free_hiburan      = $('#link_vidio_free_hiburan_edit').val();
            let link_event                   = $('#link_event_edit').val();
            let harga                        = $('#harga_edit').val();
            let diskon                       = $('#diskon_edit').val();
            let status_content               = $('#status_content_edit').val();
            let type_jenis_content           = $('#type_jenis_content_edit').val();
            let kapasitas_penonton           = $('#kapasitas_penonton_edit').val();
            var file_data = $('#image_event_type_edit').prop('files')[0];   
            var form_data = new FormData();                  
            form_data.append('image_event_type_edit', file_data);
            form_data.append('id',id);
            form_data.append('judul_event',judul_event);
            form_data.append('deskripsi_event',deskripsi_event);
            form_data.append('link_vidio_penjelasan_event',link_vidio_penjelasan_event);
            form_data.append('link_vidio_free_hiburan',link_vidio_free_hiburan);
            form_data.append('link_event',link_event);
            form_data.append('harga',harga);
            form_data.append('diskon',diskon);
            form_data.append('status_content',status_content);
            form_data.append('type_jenis_content',type_jenis_content);
            form_data.append('kapasitas_penonton',kapasitas_penonton);

             $.ajax({
              type: "post",
              url: `{{url('/api/content/update')}}`,
              dataType: 'JSON',  // what to expect back from the PHP script, if anything
              cache: false,
              contentType: false,
              processData: false,
              data: form_data,
             
              success: function (response) {
                 if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Update Data!',
                    'success'
                  )
                  $('#exampleModaledit').modal('hide');
                  DataTable();
                  $('#nama_usaha_edit').val('');                
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Update Data!',
                    'error'
                  )
                }
              }
            }); 
            // $.ajax({
            //   type: "put",
            //   url: `{{url('/api/content/update')}}`,
            //   data: form_data,
            //   dataType: "JSON",
            //   success: function (response) {
            //     if (response.status) {
            //       Swal.fire(
            //         'Berhasil!',
            //         'Berhasil Update Data!',
            //         'success'
            //       )
            //       $('#updateactivity').modal('hide');
            //       DataTable();
            //       $('#nama_usaha_edit').val('');                
                  
            //     }else{
            //       Swal.fire(
            //         'Gagal!',
            //         'Gagal Update Data!',
            //         'error'
            //       )
            //     }
            //   }
            // });   
        });
    }

     function deletedata(id) {
            $.ajax({
              type: "delete",
              url: `{{url('/api/content/delete/${id}')}}`,
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Deletes Data!',
                    'success'
                  )
                  DataTable();
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Deletes Data!',
                    'error'
                  )
                }
              }
            });    
    }

    function type_jenis_content_new() {
      // $('#type_jenis_content_new').empty();
      // $('#type_jenis_content_edit').empty();
      $.ajax({
        type: "get",
        url: `{{url('/api/jenisusaha/getDatajenisusaha')}}`,
        dataType: "JSON",
        success: function (response) {
         
          if (response.status) {
            response.data.forEach(function(item) {
              console.table(item)
               $('#type_jenis_content_new').append(
                 `<option value="${item.id}">${item.name_jenis_usaha}</option>`
               )

              //  $('#type_jenis_content_edit').append(
              //    `<option value="${item.id}">${item.name_jenis_usaha}</option>`
              //  )
             });
          }
        }
      });


    }
</script>

@endsection