<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="{{url('')}}/dashboard/assets/images/favicon.ico">

    <title>Buka Usaha</title>

    <link href="{{url('')}}/dashboard/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('')}}/dashboard/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="{{url('')}}/dashboard/assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="{{url('')}}/dashboard/assets/js/modernizr.min.js"></script>

</head>

<body>

    <div class="account-pages"></div>
    <div class="clearfix"></div>


    <div class="container-alt">
        <div class="row">
            <div class="col-sm-10 offset-sm-1">
                <div class="wrapper-page signup-signin-page">
                    <div class="card-box">
                        <div class="panel-heading">
                            <h4 class="text-center"> Welcome to <strong class="text-custom">BukaUsaha</strong></h4>
                        </div>

                        <div class="p-20">
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs tabs">
                                        <li class="nav-item tab">
                                            <a href="#home-2" data-toggle="tab" aria-expanded="false" class="nav-link active show">
                                                Sign In
                                            </a>
                                        </li>
                                        <li class="nav-item tab">
                                            <a href="#register" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                Sign Up
                                            </a>
                                        </li>

                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home-2">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="username">Username : <span style="color:red;"> *</span></label>
                                                        <input type="text" name="username" id="username" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="password">password : <span style="color:red;"> *</span></label>
                                                        <input type="text" name="password" id="password" class="form-control">
                                                    </div>


                                                    <button class="btn btn-danger">Cancel</button>
                                                    <button class="btn btn-primary">Login</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="register">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="nama">Nama : <span style="color:red;"> *</span></label>
                                                        <input type="text" name="nama" class="form-control" id="nama">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="umur">Umur : <span style="color:red;"> *</span></label>
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="jeniskelamin">Jenis Kelamin : <span style="color:red;"> *</span></label>
                                                        <select name="jeniskelamin" id="jeniskelamin" class="form-control">
                                                            <option value="">Not Selected</option>
                                                            <option value="">Perempuan</option>
                                                            <option value="">Laki-laki</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="No.Telepon">No.Telepon : <span style="color:red;"> *</span></label>
                                                        <input type="text" name="No.Telepon" class="form-control" id="nama">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="Akun Sosmed">Akun Sosmed : <span style="color:red;"> *</span></label>
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="Email">Email : <span style="color:red;"> *</span></label>
                                                        <input type="text" name="Email" class="form-control" id="nama">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="umur">Status User : <span style="color:red;"> *</span></label>
                                                        <select name="status" class="form-control">
                                                            <option value="">Status User</option>
                                                            <option value="">Pembeli</option>
                                                            <option value="">Penjual</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="foto">foto : <span style="color:red;"> *</span></label>
                                                        <input type="file" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="jenisusaha">Jenis Usaha : <span style="color:red;"> *</span></label>
                                                        <select name="jenisusaha" id="jenisusaha" class="form-control">

                                                            <option value="">Fashion</option>
                                                            <option value="">Film Animasi dan Vidio</option>
                                                            <option value="">Kerajinan Tangan</option>
                                                            <option value="">Kuliner</option>
                                                            <option value="">Musik</option>
                                                            <option value="">Seni Pertujunkan</option>
                                                            <option value="">Jual Tanaman Hias</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-9">

                                                </div>


                                                <button type="submit" style="margin: 1px;" class="btn btn-danger">Clear</button>
                                                <button type="submit" style="margin: 1px;" class="btn btn-primary">Submit</button>


                                            </div>
                                        </div>

                                    </div>
                                </div>





                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!-- jQuery  -->
    <script src="{{url('')}}/dashboard/assets/js/jquery.min.js"></script>
    <script src="{{url('')}}/dashboard/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
    <script src="{{url('')}}/dashboard/assets/js/bootstrap.min.js"></script>
    <script src="{{url('')}}/dashboard/assets/js/waves.js"></script>
    <script src="{{url('')}}/dashboard/assets/js/jquery.slimscroll.js"></script>
    <script src="{{url('')}}/dashboard/assets/js/jquery.scrollTo.min.js"></script>

    <!-- App js -->
    <script src="{{url('')}}/dashboard/assets/js/jquery.core.js"></script>
    <script src="{{url('')}}/dashboard/assets/js/jquery.app.js"></script>

</body>

</html>