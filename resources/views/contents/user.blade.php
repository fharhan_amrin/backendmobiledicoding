@extends('welcome')

@section('content')
<!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">  {{$data['title']}}</a></li>
                                    <li class="breadcrumb-item"><a href="#">  {{$data['sub_title_one']}}</a></li>
                                   
                                </ol>
                            </div>
                            <h4 class="page-title">Dashboard</h4>
                        </div>
                    </div>
                </div>
 <!-- end page title end breadcrumb -->
<div class="row">
    {{$data['title']}}
    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Autem sapiente laudantium, enim vero qui esse ex nobis sed nulla at maxime ab quidem voluptatibus similique deserunt sunt. Minima, nihil eos.
</div>
    
@endsection