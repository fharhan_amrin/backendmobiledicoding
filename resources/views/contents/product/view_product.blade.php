@extends('welcome')

@section('title')
    LapakUsahaKita.com - Manage Product
@endsection

@section('content')

<div class="container">
    <div class="row">
       <div class="col-md-12">
           <div class="card">
               <div class="card-body">
                   <div class="card-title">
                       <h1> Manage Product <a data-toggle="modal" data-target="#infomodal"><i class="fa fa-info-circle"></i></a></h1>
                   </div>
                   <div class="clearfix">
                       <div class="float-right">
                           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addactivity">
                            Tambah Product
                            </button>
                       </div>
                   </div>
                    <div class="table-responsive mt-1">    
                        <table id="data-table" class="table" width="100%"ß>
                                <thead>
                                    <th>Image Product</th>
                                    <th>Name Product </th>
                                    <th>Harga Per Item</th>
                                    <th>Stock Product</th>
                                    <th>Status Product</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                        </table>
                    </div>
               </div>
           </div>
       </div>
    </div>
</div>


{{-- awal modal --}}
<div class="modal fade" id="addactivity" tabindex="-1" aria-labelledby="addactivityLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addactivityLabel">Add Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formInsert" method="post" enctype="multipart/form-data"> 

   
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Name Product : <span style="color:red;">*</span>  </label>
                    <input type="text" id="name_product" name="name_product" class="form-control">
                </div>
            </div>

             <div class="col-md-6">
                <div class="form-group">
                    <label for="">Harga Per Item : <span style="color:red;">*</span>  </label>
                    <input type="text" id="harga_product" name="harga_product" class="form-control">
                </div>
            </div>
             <div class="col-md-6">
                <div class="form-group">
                    <label for="">Stock Product : <span style="color:red;">*</span>  </label>
                    <input type="number" id="stock_product" name="stock_product" class="form-control">
                </div>
            </div>
             <div class="col-md-6">
                <div class="form-group">
                    <label for=""> Status Product : <span style="color:red;">*</span>  </label>
                     <select name="status_product" id="status_product" class="form-control">
                       <option value="">Not Selected</option>
                       <option value="0">Tersedia</option>
                       <option value="1">Habis</option>
                       <option value="2">Segere Hadir</option>
                      </select>
                </div>
            </div>

             <div class="col-md-6">
                <div class="form-group">
                    <label for=""> Jenis Usaha : <span style="color:red;">*</span>  </label>
                     <select name="id_jenis_usaha" id="id_jenis_usaha" class="form-control">
                       <option value="">Not Selected</option>
                       
                      </select>

                      {{-- simpan name product --}}

                      <input type="hidden" name="name_jenis_usaha" id="name_jenis_usaha">
                </div>
            </div>
             <div class="col-md-6">
                <div class="form-group">
                    <label for=""> Image Product : <span style="color:red;">*</span>  </label>
                     <input type="file" name="image_product" id="image_product" class="form-control">
                </div>
            </div>
             <div class="col-md-6">
                <div class="form-group">
                    <label for=""> Deskripsi Penjelasan Product : <span style="color:red;">*</span>  </label>
                     <textarea name="deskripsi_penjelasan_product" id="deskripsi_penjelasan_product" cols="30" rows="10" class="form-control" ></textarea>
                </div>
            </div>

            
           
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
         </form>
    </div>
  </div>
</div>

{{-- akhir add --}}

{{-- awal modal --}}
<div class="modal fade" id="updateactivity" tabindex="-1" aria-labelledby="updateactivityLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateactivityLabel">Update Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formUpdate" method="post" enctype="multipart/form-data"> 

   
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Name Product : <span style="color:red;">*</span>  </label>
                     <input type="hidden" name="id" id="id_edit">
                    <input type="text" id="name_product_edit" name="name_product" class="form-control">
                </div>
            </div>

             <div class="col-md-6">
                <div class="form-group">
                    <label for="">Harga Per Item : <span style="color:red;">*</span>  </label>
                    <input type="text" id="harga_product_edit" name="harga_product" class="form-control">
                </div>
            </div>
             <div class="col-md-6">
                <div class="form-group">
                    <label for="">Stock Product : <span style="color:red;">*</span>  </label>
                    <input type="number" id="stock_product_edit" name="stock_product" class="form-control">
                </div>
            </div>
             <div class="col-md-6">
                <div class="form-group">
                    <label for=""> Status Product : <span style="color:red;">*</span>  </label>
                     <select name="status_product" id="status_product_edit" class="form-control">
                       <option value="">Not Selected</option>
                       <option value="0">Tersedia</option>
                       <option value="1">Habis</option>
                       <option value="2">Segere Hadir</option>
                      </select>
                </div>
            </div>

             <div class="col-md-6">
                <div class="form-group">
                    <label for=""> Jenis Usaha : <span style="color:red;">*</span>  </label>
                     <select name="id_jenis_usaha" id="id_jenis_usaha_edit" class="form-control">
                       <option value="">Not Selected</option>
                       
                      </select>

                      {{-- simpan name product --}}

                      <input type="hidden" name="name_jenis_usaha" id="name_jenis_usaha_edit">
                </div>
            </div>
             <div class="col-md-6">
                <div class="form-group">
                    <label for=""> Image Product : <span style="color:red;">*</span>  </label>
                    <div id="image-edit"></div>
                   
                     <input type="file" name="image_product" id="image_product_edit" class="form-control">
                </div>
            </div>
             <div class="col-md-6">
                <div class="form-group">
                    <label for=""> Deskripsi Penjelasan Product : <span style="color:red;">*</span>  </label>
                     <textarea name="deskripsi_penjelasan_product" id="deskripsi_penjelasan_product_edit" cols="30" rows="10" class="form-control" ></textarea>
                </div>
            </div>

            
           
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
         </form>
    </div>
  </div>
</div>

{{-- akhir add --}}


{{-- info modal --}}


<div class="modal fade" id="infomodal" tabindex="-1" aria-labelledby="infomodalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="infomodalLabel">Info Manage Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Selamat Datang Di Halaman Manage Product, di sini anda akan menambah product anda, meng edit product anda, dan men delete product anda</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

{{-- akhir modal --}}



<script>
    $(document).ready(function () {
        DataTable();
        AddActivityLiburan();
        UpdateActivityLiburan();
        id_jenis_usaha();
    });


    function DataTable() {
      let id = localStorage.getItem('id');
      $('#data-table').DataTable({
          processing: true,
          serverSide: true,
          destroy: true,
          orderable: false, 
          searchable: false,
          ajax:   `{{ url('/product/${id}') }}`,
          columns: [
              {data: 'image_product'},
              {data: 'name_product'},
              {data: 'harga_product'},
              {data: 'stock_product'},
              {data: 'status_product'},
              {data: 'action'},
          ]
      });
	}

    function AddActivityLiburan() {            
            $('#formInsert').submit(function (e) { 
            e.preventDefault();
            let id_user                      = localStorage.getItem('id');
            let name_product                 = $('#name_product').val();
            let harga_product                = $('#harga_product').val();
            let stock_product                = $('#stock_product').val();
            let id_jenis_usaha               = $('#id_jenis_usaha').val();
            let status_product               = $('#status_product').val();
            let name_jenis_usaha             = $('#name_jenis_usaha').val();
            let deskripsi_penjelasan_product = $('#deskripsi_penjelasan_product').val();
            var file_data = $('#image_product').prop('files')[0];   
            var form_data = new FormData();                  
            form_data.append('image_product', file_data);
            form_data.append('id_user',id_user);
            form_data.append('name_product',name_product);
            form_data.append('harga_product',harga_product);
            form_data.append('stock_product',stock_product);
            form_data.append('id_jenis_usaha',id_jenis_usaha);
            form_data.append('status_product',status_product);
            form_data.append('name_jenis_usaha',name_jenis_usaha);
            form_data.append('deskripsi_penjelasan_product',deskripsi_penjelasan_product);
            
            $.ajax({
              type: "post",
               url: `{{url('/api/product/store')}}`,
              dataType: 'JSON',  // what to expect back from the PHP script, if anything
              cache: false,
              contentType: false,
              processData: false,
              data: form_data,
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Menambah Data!',
                    'success'
                  )
                   $('#addactivity').modal('hide');
                   DataTable(); 
                }
              }
            });          
            });          
             
     
    }

    
     function getdataedit(id) {
         $('#image-edit').empty();

         
        
          $.ajax({
            type: "get",
            url: `{{url('/api/product/edit/${id}')}}`,
            dataType: "JSON",
            success: function (response) {
              let {id,id_user,image_product,name_product,harga_product,stock_product,status_product,id_jenis_usaha,name_jenis_usaha,deskripsi_penjelasan_product} = response.data;
              if (response.status) {
                $('#id_edit').val(id);
                $('#name_product_edit').val(name_product);
                $('#harga_product_edit').val(harga_product);
                $('#stock_product_edit').val(stock_product);
                $('#status_product_edit').val(status_product);
                $('#id_jenis_usaha_edit').val(id_jenis_usaha);
                $('textarea#deskripsi_penjelasan_product_edit').val(deskripsi_penjelasan_product);

                $('#image-edit').append(
           `<img src="uploads/product/${image_product}" id="image-date-edit" alt="image-edit" style="    width: 50%;">`
           );
                
              }
            }
          });

    }

    function UpdateActivityLiburan() {

        $('#formUpdate').submit(function (e) { 
            e.preventDefault();

            let id                           = $('#id_edit').val();
            let name_product                 = $('#name_product_edit').val();
            let harga_product                = $('#harga_product_edit').val();
            let stock_product                = $('#stock_product_edit').val();
            let id_jenis_usaha               = $('#id_jenis_usaha_edit').val();
            let status_product               = $('#status_product_edit').val();
            let name_jenis_usaha             = $('#name_jenis_usaha_edit').val();
            let deskripsi_penjelasan_product = $('#deskripsi_penjelasan_product_edit').val();
            var file_data = $('#image_product_edit').prop('files')[0];   
            var form_data = new FormData();                  
            form_data.append('image_product', file_data);
            form_data.append('id',id);
            form_data.append('name_product',name_product);
            form_data.append('harga_product',harga_product);
            form_data.append('stock_product',stock_product);
            form_data.append('id_jenis_usaha',id_jenis_usaha);
            form_data.append('status_product',status_product);
            form_data.append('name_jenis_usaha',name_jenis_usaha);
            form_data.append('deskripsi_penjelasan_product',deskripsi_penjelasan_product);
            $.ajax({
              type: "POST",
              url: `{{url('/api/product/update')}}`,
              data: form_data,
              dataType: "JSON",
              cache: false,
              contentType: false,
              processData: false,
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Update Data!',
                    'success'
                  )
                  $('#updateactivity').modal('hide');
                  DataTable();
                  $('#nama_usaha_edit').val('');                
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Update Data!',
                    'error'
                  )
                }
              }
            });   
        });
    }

     function deletedata(id) {
            $.ajax({
              type: "delete",
              url: `{{url('/api/product/delete/${id}')}}`,
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Deletes Data!',
                    'success'
                  )
                  DataTable();
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Deletes Data!',
                    'error'
                  )
                }
              }
            });    
    }


    function id_jenis_usaha() {
       $('#id_jenis_usaha').empty();
      $('#id_jenis_usaha_edit').empty();
      $.ajax({
        type: "get",
        url: `{{url('/api/jenisusaha/getDatajenisusaha')}}`, data: "data",
        dataType: "JSON",
        success: function (response) {
          let {status,data} = response
          if (status) {
             data.forEach(function(item) {
               $('#id_jenis_usaha').append(
                 `<option value="${item.id}" data-jeniusaha="${item.name_jenis_usaha}">${item.name_jenis_usaha}</option>`
               )

               $('#id_jenis_usaha_edit').append(
                 `<option value="${item.id}" data-jeniusaha="${item.name_jenis_usaha}">${item.name_jenis_usaha}</option>`
               )
             });
          }
        }
      });
    }
</script>


@endsection