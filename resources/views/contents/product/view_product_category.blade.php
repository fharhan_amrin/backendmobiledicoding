@extends('welcome')

@section('title')
    LapakUsahaKita.com
@endsection

@section('content')

<div class="container">
    <div class="row">
       <div class="col-md-12">
           <div class="card">
               <div class="card-body">
                   <div class="card-title">
                       <h1> Product Category <a data-toggle="modal" data-target="#infomodal"><i class="fa fa-info-circle"></i></a></h1>
                   </div>
                   <div class="clearfix">
                       <div class="float-right">
                           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addactivity">
                            Tambah Product Category
                            </button>
                       </div>
                   </div>
                    <div class="table-responsive mt-1">    
                        <table id="data-table" class="table" width="100%"ß>
                                <thead>
                                    {{-- <th>No</th> --}}
                                    <th>Name Category</th>
                                    <th>Harga Nomal</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                        </table>
                    </div>
               </div>
           </div>
       </div>
    </div>
</div>


{{-- awal modal --}}
<div class="modal fade" id="addactivity" tabindex="-1" aria-labelledby="addactivityLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addactivityLabel">Tambah Category Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formInsert" method="post">

   
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Name : <span style="color:red;">*</span>  </label>
                    <input type="text" id="name" name="name" class="form-control">
                </div>
            </div>
           
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
         </form>
    </div>
  </div>
</div>

{{-- akhir add --}}

{{-- awal modal --}}
<div class="modal fade" id="updateactivity" tabindex="-1" aria-labelledby="updateactivityLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateactivityLabel">Update Category Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formUpdate" method="post">

   
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Name : <span style="color:red;">*</span> </label>
                    <input type="text" id="name" class="form-control" name="name">
                </div>
            </div>
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
         </form>
    </div>
  </div>
</div>

{{-- akhir add --}}


{{-- info modal --}}


<div class="modal fade" id="infomodal" tabindex="-1" aria-labelledby="infomodalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="infomodalLabel">Info Product Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo, sequi eius. Reprehenderit soluta alias aspernatur atque eligendi perspiciatis repellat laboriosam doloremque in mollitia suscipit saepe, repellendus molestias cupiditate id quidem!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

{{-- akhir modal --}}



<script>
    $(document).ready(function () {
        DataTable();
        AddActivityLiburan();
        UpdateActivityLiburan();
    });


    function DataTable() {
      $('#data-table').DataTable({
          processing: true,
          serverSide: true,
          destroy: true,
          orderable: false, 
          searchable: false,
          ajax: "{{ url('/productcategory') }}",
          columns: [
              {data: 'name_category'},
              {data: 'harga_normal'},
              // {data: 'updated_at'},
              {data: 'action'},
          ]
      });
	}

    function AddActivityLiburan() {
        $('#formInsert').submit(function (e) { 
            e.preventDefault();
            $.ajax({
              type: "post",
              url: `{{url('/api/jenisusaha/store')}}`,
              data: $('#formInsert').serialize(),
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Menambah Data!',
                    'success'
                  )
                   $('#addactivity').modal('hide');
                   DataTable(); 
                }
              }
            });           
        });
    }

    
     function getdataedit(id) {
        
          $.ajax({
            type: "get",
            url: `{{url('/api/jenisusaha/edit/${id}')}}`,
            dataType: "JSON",
            success: function (response) {
              let {name_jenis_usaha,id} = response.data;
              if (response.status) {
                $('#id').val(id);
                $('#name_jenis_usaha_edit').val(name_jenis_usaha);
              }
            }
          });

    }

    function UpdateActivityLiburan() {
        $('#formUpdate').submit(function (e) { 
            e.preventDefault();
            $.ajax({
              type: "put",
              url: `{{url('/api/jenisusaha/update')}}`,
              data: $('#formUpdate').serialize(),
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Update Data!',
                    'success'
                  )
                  $('#updateactivity').modal('hide');
                  DataTable();
                  $('#nama_usaha_edit').val('');                
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Update Data!',
                    'error'
                  )
                }
              }
            });   
        });
    }

     function deletedata(id) {
            $.ajax({
              type: "delete",
              url: `{{url('/api/jenisusaha/delete/${id}')}}`,
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Deletes Data!',
                    'success'
                  )
                  DataTable();
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Deletes Data!',
                    'error'
                  )
                }
              }
            });    
    }
</script>

{{-- akhir modal --}}
@endsection