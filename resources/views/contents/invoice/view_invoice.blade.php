@extends('welcome')

@section('title')
   LapakUsahaKita.com - Invoice History
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h1>Invoice History <a data-toggle="modal" data-target="#infomodal"><i class="fa fa-info-circle"></i></a></h1>
                        
                    </div>
                   
                    <div class="table-responsive mt-1">
                        <table id="data-table" class="table" width="100%">
                            <thead>
                                <tr>
                                    <th>Invoice</th>
                                    <th>Invoice Date</th>
                                    <th>Due Date</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              
                                    
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



{{-- edit modal--}}

<div class="modal fade" id="exampleModaledit" tabindex="-1" aria-labelledby="exampleModaleditLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModaleditLabel">Update Bukti Pembayaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="javascript:void(0)" id="formUpdate" method="post" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Foto file bukti : <span style="color:red;">*</span></label>
                    <input type="file" class="form-control" id="image_file_bukti_pembayaran_edit" name="image_file_bukti_pembayaran_edit" >
                    <input type="hidden" name="id" id="id_edit">
                </div>
            </div>
            

             

         
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
      </form>
    </div>
  </div>
</div>
{{-- akhir edit modal --}}



{{-- info modal --}}


<div class="modal fade" id="infomodal" tabindex="-1" aria-labelledby="infomodalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="infomodalLabel">Info Contents</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
        <p>Selamat Datang Di Halaman Content event , di sini anda akan  memasukan produk anda/usaha anda , di sini anda akan membuat event, penjelasan , tutorial, tips , trick  sesuai bidang jenis usaha anda ,secara gratis ataupun ber bayar untuk di promosikan di seluruh indonesia  berbentuk vidio, atau secara live streaming menggunakan google meet, live instagram, live youtube , vidio google drive dll  </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

{{-- akhir modal --}}



<script>
    $(document).ready(function () {
      console.log("id_user"+localStorage.getItem('id'));
        DataTable();
        AddActivityLiburan();
        
    });


    function DataTable() {
      let id = localStorage.getItem('id');
      $('#data-table').DataTable({
          processing: true,
          serverSide: true,
          destroy: true,
          orderable: false, 
          searchable: false,
          ajax: `{{ url('/invoice/${id}') }}`,
          columns: [
              {data: 'invoice'},
              {data: 'invoice_start'},
              {data: 'invoice_end'},
              {data: 'total'},
              {data: 'status'},
              {data: 'action'},
          ]
      });
	}

    function AddActivityLiburan() {
        $('#formUpdate').submit(function (e) { 
            e.preventDefault();
            var id_user    = $('#id_edit').val();
            var file_data = $('#image_file_bukti_pembayaran_edit').prop('files')[0];   
            var form_data = new FormData();                  
            form_data.append('image_file_bukti_pembayaran_edit', file_data);
            form_data.append('id',id_user);
           
            $.ajax({
              type: "post",
              url: `{{url('/api/invoicehistory/store')}}`,
              dataType: 'JSON',  // what to expect back from the PHP script, if anything
              cache: false,
              contentType: false,
              processData: false,
              data: form_data,
             
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Upload Bukti Pembayaran Data!',
                    'success'
                  )
                   $('#exampleModaledit').modal('hide');
                   DataTable(); 
                }
              }
            });           
        });
    }

    

    
     function getdataedit(id) {
        
          $.ajax({
            type: "get",
            url: `{{url('/api/content/edit_invoice/${id}')}}`,
            dataType: "JSON",
            success: function (response) {
              let {id} = response.data;
              if (response.status) {
                $('#id_edit').val(id);
                
              }
            }
          });

    }

    

     function deletedata(id) {
            $.ajax({
              type: "delete",
              url: `{{url('/api/content/delete/${id}')}}`,
              dataType: "JSON",
              success: function (response) {
                if (response.status) {
                  Swal.fire(
                    'Berhasil!',
                    'Berhasil Deletes Data!',
                    'success'
                  )
                  DataTable();
                  
                }else{
                  Swal.fire(
                    'Gagal!',
                    'Gagal Deletes Data!',
                    'error'
                  )
                }
              }
            });    
    }

</script>

@endsection