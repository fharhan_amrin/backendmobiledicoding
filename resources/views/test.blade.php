@extends('welcome')

@section('title')
    Buka Usaha - Profil
@endsection

@section('content')

  <div class="container">
	  <div class="row">
		  <div class="col-md-12">
			  <div class="table-responsive">
				  <table class="table table-bordered data-table">
						<thead>
							<tr>
								<th>No</th>
								<th>username</th>
								<th>Email</th>
								<th width="100px">Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
			  </div>
		  </div>
	  </div>
  </div>



<script>

	$(document).ready(function () {
		DataTable();
	});


	function DataTable() {
		$('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('/test') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'username', name: 'username'},
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action',
			 orderable: false, 
			 searchable: false
			 },
        ]
    });
	}


	function ucapkan() {
		alert("ucapkan");
	  }
</script>

@endsection