
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="{{ url('')}}/satu.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>LapakUsahaKita.com</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
         <meta name="dicoding:email" content="fharhanamrin028@gmail.com">

        <link href="{{ url('')}}/assets/css/bootstrap.css" rel="stylesheet" />
        <link href="{{ url('')}}/assets/css/landing-page.css" rel="stylesheet"/>
        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
        <link href="{{ url('')}}/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
        <style>
             @font-face {
        font-family: myFirstFont;
        src: {{url('roboto-slab/RobotoSlab-Bold.ttf')}};
        }
        h1,h2,h3,h4,h5,h6,p,b{
          font-family: myFirstFont ;
        }
        </style>
    </head>
    <body class="landing-page landing-page1">
        <nav class="navbar navbar-transparent navbar-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button id="menu-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                    </button>
                    <a href="http://www.creative-tim.com">
                        <div class="logo-container">
                            {{-- <div class="logo">
                                <img src="{{ url('')}}/assets/img/new_logo.png" alt="Creative Tim Logo">
                            </div>
                            <div class="brand">
                               BUC 19
                            </div> --}}
                        </div>
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="example" >
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a class="btn btn-primary" href={{route('registeruser')}} style="color: white;">
                            Login
                            </a>
                            
                        </li>
                       
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
        </nav>
        <div class="wrapper">
            <div class="parallax filter-gradient orange" data-color="orange" style="background: linear-gradient(#e66465, blue);">
                <div class="parallax-background">
                    <img class="parallax-background-image" src="{{ url('')}}/satu.png">
                </div>
                <div class= "container">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="description">
                                <b><h2>LapakUsahaKita.com</h2></b>
                                <br>
                                <p> Lapak Usaha kita adalah tempat ber kumpulnya semua jenis bidang usaha, untuk menjual dan mempromosikan productnya atau jasanya sesuai bidang anda, bukan hanya itu !!!</p>
                                <p>Di sini juga ada fitur untuk management Aktivity liburan keluarga anda ! menunggu virus covid 19 nya menghilang, mari kita rencanakan , usahakan dan persiapkan semuanya di  <b>LapakUsahaKita.com</b> </p>
                               
                            </div>
                            
                        </div>
                        <div class="col-md-5  hidden-xs">
                            <div class="parallax-image">
                                <img class="phone" src="{{ url('')}}/satu.png"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <div class="section section-presentation">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="description">
                                <h4>Mudah di Pahami dan di Gunakan.</h4>
                                <p>Di Tahun 2020 semua orang terkena imbas dari sebuah virus yang berasal dari china, tepatnya di kota wuhan, efek dari virus tersebut mengakibatkan semua orang terkena dampaknya, virus ini di sebut covid19, yang mengakibatkan phk dimana-mana, semua orang kehilangan pekerjaan, semua bidang usaha mengalami penurunan pendapatan, namun kita harus tetap semangat ! bangkit bersama LapakUsahaKita.com</p>
                                <p>
                                    LapakUsahaKita.com adalah tempat usaha kita semua, untuk mem promosikan bisnis,usaha, dan sebagai mengatur rencana aktifitas liburan kita dll, ketika virus sudah tidak ada.

                                </p>
                                <p>Bagi anda yang tidak memiliki usaha ? marilah mulai usaha anda, dengan banyak cara untuk mulai usaha dan mulailah banyak usaha untuk Perubahan.</p> 
                            </div>
                        </div>
                        <div class="col-md-5 col-md-offset-1 hidden-xs">
                            <img src="{{ url('')}}/satu.png"/ style="top:-30px">
                        </div>
                    </div>
                </div>
            </div>
            <h4 class="text-center">JENIS USAHA/TOPIK YANG TERSEDIA</h4>
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" >LapakUsahaKita.com</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                       <p id="data-deskripsi"> </p>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>
            <div class="container">
            
                    <div class="row" id="data-content">
                        
                    </div>
                    
                     
             
            </div>
           
           

               
            <div class="section section-features">
                <div class="container">
                    <h4 class="header-text text-center">Features</h4>
                    <div class="row">

                         <div class="col-md-4">
                            <div class="card card-orange">
                               
                                <div class="text" style="color: black;">
                                   <b> <h4><b>Management Liburan</b> </h4> </b>
                                    <p>Di sini Tempat anda untuk membuat rencana liburan secara tepat dan aman, tersedia list" barang" default yang biasa di bawa untuk jalan", tersedia juga aktifitas default dari  saran LapakUsahaKita, dan ada Notifikasi pengingat setiap aktifity liburan rencana anda, lebih detailnya bisa anda coba sendiri. </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="color: black;">
                            <div class="card card-orange">
                               
                                <div class="text">
                                   <b> <h4><b>Management Content</b></h4> </b>
                                    <p>Di Sini Tempat Para pemilik usaha content untuk mempromosikan jenis bidang usaha mereka, yang ingin di promosikan dan di jual secara free atau ber bayar.</p>
                                </div>
                            </div>
                        </div>

                             <div class="col-md-4" style="color: black;">
                            <div class="card card-orange">
                               
                                <div class="text">
                                   <b> <h4><b>Management Product</b></h4> </b>
                                    <p>Di sini Tempat Para Pemilik Usaha Me Manage Product detailnya.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="color: black;">
                            <div class="card card-orange">
                               
                                <h4><b>Smart Notifications</b></h4>
                                <p>Notifikasi email dan Bot Telegram Aktifity Liburan, bisa custum sendiri.</p>
                            </div>
                        </div>
                        <div class="col-md-4" style="color: black;">
                            <div class="card card-orange">
                                
                                <h4><b>Reporting</b></h4>
                                <p>Bagi anda seorang penjual, tersedia reporting untuk product anda, management product, stock product, management transaksi history user berupa garfik , report pdf dan excel dan di lengkapi fitur filter data start date dan enddate dll masih banyak lagi.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-testimonial
            " style="background: #b92b27;  /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #1565C0, #b92b27);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to right, #1565C0, #b92b27); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

    color: white;
">
                <div class="container">
                    <h2 class="header-text text-center" style="color: white"><b>Motivasi</b></h2>
                    <div id="carousel-example-generic" class="carousel fade" data-ride="carousel">
                        <!-- Wrapper for slides -->

                        <div class="container">
                                    <h2 style="text-align: center;color:white;" >
        "Terus berjuang, bergerak, belajar, berkarya, bekerja"
        </h2>
                        </div>
                       
                        <ol class="carousel-indicators carousel-indicators-orange">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            
                        </ol>
                    </div>
                </div>
            </div>
         
            <footer class="footer" style="background: #b92b27;  /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #1565C0, #b92b27);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to right, #1565C0, #b92b27); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
">
                
            </footer>
        </div>

    </div>
    </body>
    <script src="{{ url('')}}/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="{{ url('')}}/assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
    <script src="{{ url('')}}/assets/js/bootstrap.js" type="text/javascript"></script>
    <script src="{{ url('')}}/assets/js/awesome-landing-page.js" type="text/javascript"></script>

    <script>

         $(document).ready(function () {
             getData();
         });


         function getData() {
             $.ajax({
                 type: "get",
                 url: `{{url('/api/jenisusaha/getDatajenisusaha')}}`,
                 dataType: "JSON",
                 success: function (response) {
                     if (response.status) {
                         response.data.forEach(function(item) {
                              $('#data-content').append(
                             `<div class="col-md-3" style="margin: 1px;">
                              <button onclick="ondetail(${item.id})" style="width: 100% !important;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                ${item.name_jenis_usaha}  
                            </button>
                        </div>`
                         );
                         });
                        
                     } 
                 }
             });
         }
        function ondetail(id) {
            
            $.ajax({
                type: "get",
                url: `{{url('/api/jenisusaha/edit/')}}/${id}`,
                dataType: "JSON",
                success: function (response) {
                    
                    if (response.status) {
                        console.log(response.data.name_jenis_usaha);
                        $('.modal-title').html(response.data.name_jenis_usaha);
                        $('#data-deskripsi').html(response.data.deskripsi);
                    }
                }
            });
        }
    </script>
</html>
